#pragma once

#include "Timer.h"

/**
 * @class VirtualTimer
 *
 * Virtual timer, to analyse the effect of (fix) frequencies/variances
 * on some parts of the system.
 *
 * Time is increased everytime the function "Elapsed()" is called (i.e.
 * time represents the number of "Elapsed"-calls).
 *
 * @author Boris Schauerte
 */
class VirtualTimer
  : public Timer
{
public:
  VirtualTimer(double _timeStep);
  virtual ~VirtualTimer(void);

  void Start();
  void Restart(void);
  double Stop(void);
  double Elapsed(void);
  double Time(void);

  double SysTime(void);
protected:
private:
  double time;
  double timeSys;
  double timeStep;
  double timeStopped;
};
