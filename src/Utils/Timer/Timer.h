#pragma once

#include <stdlib.h>

#ifndef INTERFACE
#if defined _WIN32 || defined WIN32
#define INTERFACE struct __declspec(novtable)
#else
#define INTERFACE struct
#endif
#if defined _WIN32 || defined WIN32
#define ABSTRACT class __declspec(novtable)
#else
#define ABSTRACT class
#endif
#endif

/**
 * @class Timer
 *
 * Interface for timer implementations, which can differ in implementation
 * details (e.g. supported accuracy).
 *
 * @author Boris Schauerte
 */
#if defined _WIN32 || defined WIN32
INTERFACE Timer
#else
ABSTRACT Timer
#endif
{
public:
#if !defined _WIN32 && !defined WIN32
  virtual ~Timer(void) = 0;
#endif

  /** Start the timer. */
  virtual void Start(void) = 0;
  /** Restart the timer. */
  virtual void Restart(void) = 0;
  /** Stop the time. The time is stored and can be accessed by Time(). */
  virtual double Stop(void) = 0;
  /** Returns the elapsed time (in seconds) since the last Start/Restart-call. */
  virtual double Elapsed(void) = 0;
  /** Returns the elapsed time (in seconds) of the last Stop-call. */
  virtual double Time(void) = 0;

  /** Return the current time of the system. */
  virtual double SysTime(void) = 0;
};
