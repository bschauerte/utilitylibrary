#include "_ul_base_def.h"
#include "HighResTimer.h"

HighResTimer::HighResTimer(void)
{
#ifdef WIN32
  QueryPerformanceFrequency(&frequency);
  startTime.QuadPart = 0;
  endTime.QuadPart = 0;
#else
  startTime.tv_sec = startTime.tv_usec = 0;
  endTime.tv_sec = endTime.tv_usec = 0;
#endif

  Start();
  endTimeInMicroSec = startTimeInMicroSec;
}

HighResTimer::~HighResTimer(void)
{
}

void
HighResTimer::Start()
{
  stopped = false;

#ifdef WIN32
  QueryPerformanceCounter(&startTime);
  startTimeInMicroSec = startTime.QuadPart * (1000000.0 / frequency.QuadPart);
#else
  gettimeofday(&startTime, NULL);
  startTimeInMicroSec = (startTime.tv_sec * 1000000.0) + startTime.tv_usec;
#endif
}

void
HighResTimer::Restart(void)
{
  stopped = false;

#ifdef WIN32
  QueryPerformanceCounter(&startTime);
  startTimeInMicroSec = startTime.QuadPart * (1000000.0 / frequency.QuadPart);
#else
  gettimeofday(&startTime, NULL);
  startTimeInMicroSec = (startTime.tv_sec * 1000000.0) + startTime.tv_usec;
#endif

}

double
HighResTimer::Stop(void)
{
  stopped = true;

#ifdef WIN32
  QueryPerformanceCounter(&endTime);
  endTimeInMicroSec = endTime.QuadPart * (1000000.0 / frequency.QuadPart);
#else
  gettimeofday(&endTime, NULL);
  endTimeInMicroSec = (endTime.tv_sec * 1000000.0) + endTime.tv_usec;
#endif

  return (endTimeInMicroSec - startTimeInMicroSec) / 1000000.0;
}

double
HighResTimer::Elapsed(void)
{
#ifdef WIN32
  QueryPerformanceCounter(&endTime);
  double _endTimeInMicroSec = endTime.QuadPart * (1000000.0 / frequency.QuadPart);
#else
  gettimeofday(&endTime, NULL);
  double _endTimeInMicroSec = (endTime.tv_sec * 1000000.0) + endTime.tv_usec;
#endif

  return (_endTimeInMicroSec - startTimeInMicroSec) / 1000000.0;
}

double
HighResTimer::Time(void)
{
  return (endTimeInMicroSec - startTimeInMicroSec) / 1000000.0;
}

double
HighResTimer::SysTime(void)
{
#ifdef WIN32
  LARGE_INTEGER ct;
#else
  timeval ct;
#endif

#ifdef WIN32
  QueryPerformanceCounter(&ct);
  double _ctInMicroSec = ct.QuadPart * (1000000.0 / frequency.QuadPart);
#else
  gettimeofday(&ct, NULL);
  double _ctInMicroSec = (ct.tv_sec * 1000000.0) + ct.tv_usec;
#endif

  return _ctInMicroSec / 1000000.0;
}

double
HighResTimer::_SysTime(void)
{
#ifdef WIN32
  LARGE_INTEGER frequency;
  LARGE_INTEGER ct;
#else
  timeval ct;
#endif

#ifdef WIN32
  QueryPerformanceFrequency(&frequency);
  QueryPerformanceCounter(&ct);
  double _ctInMicroSec = ct.QuadPart * (1000000.0 / frequency.QuadPart);
#else
  gettimeofday(&ct, NULL);
  double _ctInMicroSec = (ct.tv_sec * 1000000.0) + ct.tv_usec;
#endif

  return _ctInMicroSec / 1000000.0;
}

boost::uint64_t
HighResTimer::_SysTimeUI64(void)
{
#ifdef WIN32
  LARGE_INTEGER frequency;
  LARGE_INTEGER ct;
#else
  timeval ct;
#endif

#ifdef WIN32
  QueryPerformanceFrequency(&frequency);
  QueryPerformanceCounter(&ct);
  boost::uint64_t _ctInMicroSec = (boost::uint64_t)(ct.QuadPart * (1000000.0 / frequency.QuadPart));
#else
  gettimeofday(&ct, NULL);
  boost::uint64_t  _ctInMicroSec = (((boost::uint64_t)ct.tv_sec) * ((boost::uint64_t)1000000)) + (boost::uint64_t)ct.tv_usec;
#endif

  return _ctInMicroSec;
}
