#pragma once

#include "Timer.h"

#define _OMP_BENCHMARK(_NAME, _FUNCTION, _NUM_RUNS) {\
    std::cout << _NAME << " Code" << " @ numRuns=" << _NUM_RUNS;\
    OMPTimer __t;\
    __t.Start();\
    for (size_t r = 0; r < _NUM_RUNS; r++)\
    {\
      _FUNCTION;\
    }\
    __t.Stop();\
    std::cout << " ... " << __t.Time() << "s" << std::endl;\
  }


/**
 * @class OMPTimer
 *
 * Timer based on the OMP omp_get_wtime() method, which has normally
 * a high resolution. You can get the resolution via "omp_get_wtick()".
 *
 * For interface description see Timer.
 *
 * @author Boris Schauerte
 */
class OMPTimer
  : public Timer
{
public:
  OMPTimer(void);
  virtual ~OMPTimer(void);

  void Start();
  void Restart(void);
  double Stop(void);
  double Elapsed(void);
  double Time(void);

  double SysTime(void);
  static double _SysTime(void);
protected:
private:
  double startTime;
  double stopTime;
  bool stopped;
};
