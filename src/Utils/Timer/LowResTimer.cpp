#include "_ul_base_def.h"
#include "LowResTimer.h"

#include <ctime>

LowResTimer::LowResTimer(void)
: time(-1.0)
{
}

LowResTimer::~LowResTimer(void)
{
}

void
LowResTimer::Start(void)
{
  timer.restart();
}

void
LowResTimer::Restart(void)
{
  timer.restart();
}

double
LowResTimer::Stop(void)
{
  time = timer.elapsed();
  return time;
}

double
LowResTimer::Elapsed(void)
{
  return timer.elapsed();
}

double
LowResTimer::Time(void)
{
  return time;
}

double
LowResTimer::SysTime(void)
{
  return ((double)std::clock()) / (double)CLOCKS_PER_SEC;
}

double
LowResTimer::_SysTime(void)
{
  return ((double)std::clock()) / (double)CLOCKS_PER_SEC;
}
