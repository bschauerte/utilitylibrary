#pragma once

#include "Timer.h"

#include "boost/cstdint.hpp"

#ifdef WIN32
#include <windows.h> // Windows system specific
#else
#include <sys/time.h> // Unix based system specific
#endif

#define _HIGH_RES_BENCHMARK(_NAME, _FUNCTION, _NUM_RUNS) {\
    std::cout << _NAME << " Code" << " @ numRuns=" << _NUM_RUNS;\
    HighResTimer __t;\
    __t.Start();\
    for (size_t r = 0; r < _NUM_RUNS; r++)\
    {\
      _FUNCTION;\
    }\
    __t.Stop();\
    std::cout << " ... " << __t.Time() << "s" << std::endl;\
  }

#define _HIGH_RES_BENCHMARK_RESULT(_STORE_RESULT, _NAME, _FUNCTION, _NUM_RUNS) {\
    std::cout << _NAME << " Code" << " @ numRuns=" << _NUM_RUNS;\
    HighResTimer __t;\
    __t.Start();\
    for (size_t r = 0; r < _NUM_RUNS; r++)\
    {\
      _STORE_RESULT = _FUNCTION;\
    }\
    __t.Stop();\
    std::cout << " ... " << __t.Time() << "s" << std::endl;\
  }


/**
 * @class HighResTimer
 *
 * Timer with high resolution accuracy (microseconds). Requires
 * platform dependent code.
 *
 * For interface description see Timer.
 *
 * @author Boris Schauerte
 */
class HighResTimer
  : public Timer
{
public:
  HighResTimer(void);
  virtual ~HighResTimer(void);

  void Start();
  void Restart(void);
  double Stop(void);
  double Elapsed(void);
  double Time(void);

  double SysTime(void);
  static double _SysTime(void); // returns system time in sec
  static boost::uint64_t _SysTimeUI64(void); // returns system time in usec
protected:
private:
  double startTimeInMicroSec; // starting time in micro-second
  double endTimeInMicroSec; // ending time in micro-second
  bool stopped; // stop flag 

#ifdef WIN32
  LARGE_INTEGER frequency;
  LARGE_INTEGER startTime;
  LARGE_INTEGER endTime;
#else
  timeval startTime;
  timeval endTime;
#endif
};
