#include "_ul_base_def.h"
#include "VirtualTimer.h"

VirtualTimer::VirtualTimer(double _timeStep)
: time(0), timeSys(0), timeStep(_timeStep), timeStopped(0)
{
}
VirtualTimer::~VirtualTimer(void)
{
}

void
VirtualTimer::Start(void)
{
  Restart();
}

void
VirtualTimer::Restart(void)
{
  time = 0;
}

double
VirtualTimer::Stop(void)
{
  timeStopped = time;
  return timeStopped;
}

double
VirtualTimer::Elapsed(void)
{
  timeSys += timeStep;
  time += timeStep;
  return time;
}

double
VirtualTimer::Time(void)
{
  return timeStopped;
}

double
VirtualTimer::SysTime(void)
{
  return timeSys;
}
