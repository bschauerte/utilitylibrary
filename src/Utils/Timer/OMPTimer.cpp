#include "_ul_base_def.h"
#include "OMPTimer.h"

#ifdef _OPENMP
#include "omp.h"
#endif

#include <iostream> // ... for std::logic_error ...
#include <stdexcept>

OMPTimer::OMPTimer(void)
{
#ifdef _OPENMP
  Start();
  stopTime = startTime;
#else
  /* Allow compiling, but don't allow object creation if there is no OpenMP support. */
  startTime = 0;
  stopTime = 0;
  throw std::logic_error("OMPTimer: No OpenMP support!");
#endif
}

OMPTimer::~OMPTimer(void)
{
}

void
OMPTimer::Start()
{
#ifdef _OPENMP
  stopped = false;
  startTime = omp_get_wtime();
#endif
}

void
OMPTimer::Restart(void)
{
#ifdef _OPENMP
  stopped = false;
  startTime = omp_get_wtime();
#endif
}

double
OMPTimer::Stop(void)
{
#ifdef _OPENMP
  stopped = true;
  stopTime = omp_get_wtime();

  return (stopTime - startTime);
#else
  return -1.0;
#endif
}

double
OMPTimer::Elapsed(void)
{
#ifdef _OPENMP
  double time = omp_get_wtime();

  return (time - startTime);
#else
  return -1.0;
#endif
}

double
OMPTimer::Time(void)
{
#ifdef _OPENMP
  return (stopTime - startTime);
#else
  return -1.0;
#endif
}

double
OMPTimer::SysTime(void)
{
#ifdef _OPENMP
  return omp_get_wtime();
#else
  return -1.0;
#endif
}

double
OMPTimer::_SysTime(void)
{
#ifdef _OPENMP
  return omp_get_wtime();
#else
  return -1.0;
#endif
}
