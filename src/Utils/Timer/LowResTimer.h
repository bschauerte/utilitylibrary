#pragma once

#include "Timer.h"

#include "boost/timer.hpp"

#define _LOW_RES_BENCHMARK(_NAME, _FUNCTION, _NUM_RUNS) {\
    std::cout << _NAME << " Code" << " @ numRuns=" << _NUM_RUNS;\
    LowResTimer __t;\
    __t.Start();\
    for (size_t r = 0; r < _NUM_RUNS; r++)\
    {\
      _FUNCTION;\
    }\
    __t.Stop();\
    std::cout << " ... " << __t.Time() << "s" << std::endl;\
  }

/**
 * @class LowResTimer
 *
 * Actual Low-resolution timer to allow easy benchmarking.
 * Based on the boost::timer implementation, which has a low resolution,
 * but is available on most platforms.
 *
 * For interface description see Timer.
 *
 * @author Boris Schauerte
 */
class LowResTimer
  : public Timer
{
public:
  LowResTimer(void);
  virtual ~LowResTimer(void);

  void Start(void);
  void Restart(void);
  double Stop(void);
  double Elapsed(void);
  double Time(void);

  double SysTime(void);
  static double _SysTime(void);
protected:
private:
  boost::timer timer;
  double time;
};
