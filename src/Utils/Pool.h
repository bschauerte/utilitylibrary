/**
 * Copyright 2009 B. Schauerte. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 *    1. Redistributions of source code must retain the above copyright 
 *       notice, this list of conditions and the following disclaimer.
 * 
 *    2. Redistributions in binary form must reproduce the above copyright 
 *       notice, this list of conditions and the following disclaimer in 
 *       the documentation and/or other materials provided with the 
 *       distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY B. SCHAUERTE ''AS IS'' AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL B. SCHAUERTE OR CONTRIBUTORS BE LIABLE 
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *  
 * The views and conclusions contained in the software and documentation
 * are those of the authors and should not be interpreted as representing 
 * official policies, either expressed or implied, of B. Schauerte.
 */

#pragma once

#include "_ul_base_def.h"

#include <list>

#include "boost/shared_ptr.hpp"
#include "boost/thread/thread.hpp"
#include "boost/thread/mutex.hpp"

#define _DEFAULT_POOL_DELETE_THRESHOLD (100)

#define _SAFE_POOL_RELEASE(POOL,PTR) { if (PTR != NULL) POOL.Release(PTR); PTR = NULL; }

/**
 * @class PoolObjectCreator
 *
 * Enable the use of non-default constructors in the pool class.
 *
 * @author Boris Schauerte
 * @date 2009
 */
template <class T>
class PoolObjectCreator
{
public:
  PoolObjectCreator(void)
  {
  }
  virtual ~PoolObjectCreator(void)
  {
  }
  virtual T* Create(void) = 0;
protected:
private:
};

/**
 * @class Pool
 *
 * Class to systematically reuse items to avoid unnecessary new/delete operations.
 * Can significantly improve performance if large memory allocations can be avoided.
 *
 * @author Boris Schauerte
 * @date 2009
 */
template <class T>
class Pool
{
public:
  Pool(void)
    : poc(NULL)
  {
  }
  Pool(PoolObjectCreator<T>* _poc)
    : poc(_poc)
  {
  }
  virtual ~Pool(void)
  {
    SAFE_DELETE(poc);
  }

  virtual T* Create(void) = 0;
  virtual void Release(T*& item) = 0;

  void SetObjectCreator(PoolObjectCreator<T>* _poc)
  {
    SAFE_DELETE(poc);
    poc = _poc;
  }
  PoolObjectCreator<T>* GetObjectCreator(void)
  {
    return poc;
  }
protected:
  PoolObjectCreator<T>* poc;
  T* _Create(void)
  {
    if (poc)
      return poc->Create();
    else
      return new T();
  }
private:
};

/**
 * @class DummyPool
 *
 * DummyPool is a new/delete wrapper that fits into the Pool interface.
 *
 * @author Boris Schauerte
 * @date 2009
 */
template <class T>
class DummyPool
  : public Pool<T>
{
public:
  DummyPool(void)
    : Pool<T>()
  {
  }
  virtual ~DummyPool(void)
  {
  }
  virtual T* Create(void)
  {
    return this->_Create();
  }
  virtual void Release(T*& item)
  {
    delete item;
    item = NULL;
  }
protected:
private:
};

/**
 * @class BasePool
 *
 * BasePool is a simple Pool implementation.
 * The number of max. unused items in the system is limited by 'deleteThreshold'.
 *
 * @author Boris Schauerte
 * @date 2009
 */
/*
 *
 * Note:
 * - implementation is not thread safe, because Create and Release
 *   can be called out of different threads
 */
template <class T>
class BasePool
  : public Pool<T>
{
public:
  BasePool(void)
    : Pool<T>(), deleteThreshold(_DEFAULT_POOL_DELETE_THRESHOLD)
  {
  }
  BasePool(size_t _deleteThreshold)
    : Pool<T>(), deleteThreshold(_deleteThreshold)
  {
  }
  virtual ~BasePool(void)
  {
  }
  virtual T* Create(void)
  {
    if (unusedElements.empty())
    {
      T* tmp = this->_Create();
      usedElements.push_back(tmp);
      return tmp;
    }
    else
    {
      T* tmp = unusedElements.front();
      unusedElements.pop_front();
      usedElements.push_back(tmp);
      return tmp;
    }
  }
  virtual void Release(T*& item)
  {
    if (unusedElements.size() > deleteThreshold)
    {
      usedElements.remove(item);
      delete item;
      item = 0;
    }
    else
    {
      usedElements.remove(item);
      unusedElements.push_back(item);
    }
  }
protected:
  std::list<T*> usedElements;
  std::list<T*> unusedElements;
private:
  size_t deleteThreshold; // avoid unlimited growths of the pool
};

/**
 * @class BasePool
 *
 * Thread safe BasePool wrapper.
 *
 * @author Boris Schauerte
 * @date 2009
 */
template <class T>
class BasePool_TS
  : public BasePool<T>
{
public:
  BasePool_TS(void)
    : BasePool<T>()
  {
  }
  BasePool_TS(size_t _deleteThreshold)
    : BasePool<T>(_deleteThreshold)
  {
  }
  virtual ~BasePool_TS(void)
  {
  }
  virtual T* Create(void)
  {
    boost::mutex::scoped_lock lock(mutex);
    return BasePool<T>::Create();
  }
  virtual void Release(T*& item)
  {
    boost::mutex::scoped_lock lock(mutex);
    BasePool<T>::Release(item);
  }
protected:
  mutable boost::mutex mutex;
};
