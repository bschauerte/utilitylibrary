/**
 * Copyright 2009 B. Schauerte. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 *    1. Redistributions of source code must retain the above copyright 
 *       notice, this list of conditions and the following disclaimer.
 * 
 *    2. Redistributions in binary form must reproduce the above copyright 
 *       notice, this list of conditions and the following disclaimer in 
 *       the documentation and/or other materials provided with the 
 *       distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY B. SCHAUERTE ''AS IS'' AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL B. SCHAUERTE OR CONTRIBUTORS BE LIABLE 
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *  
 * The views and conclusions contained in the software and documentation
 * are those of the authors and should not be interpreted as representing 
 * official policies, either expressed or implied, of B. Schauerte.
 */

#pragma once

#ifndef _WIN32 || WIN32

#include <stdexcept>
#include <string>

#include <stddef.h>
#include <dlfcn.h>

#define _DEFAULT_AUTO_CLOSE_HANDLE (true)

typedef void* Handle_t; // the handle type

/**
 * @class LibraryLoaderException
 *
 * C++ exception used by the LibraryLoader - with support for a custom what message.
 *
 * @author Boris Schauerte
 * @date 2009
 */
class LibraryLoaderException
: public std::exception
{
public:
  LibraryLoaderException(const std::string& _what)
  : _what_msg(_what)
  {
  }
  virtual ~LibraryLoaderException(void) throw()
  {
  }
  virtual const char* what() const throw()
  {
    return _what_msg.c_str();
  }
protected:
  std::string _what_msg;
private:
};

/** Load the symbol; w/o casting to the wished symbol type */
inline void* _LoadSymbol(Handle_t handle, const char* name) throw(LibraryLoaderException)
{
  void* sym = dlsym(handle, name);
  // Since the value of the symbol
  // could actually be NULL (so that a NULL return  from  dlsym()  need  not
  // indicate  an  error),  the  correct way to test for an error is to call
  // dlerror() to clear any old error conditions,  then  call  dlsym(),  and
  // then call dlerror() again, saving its return value into a variable, and
  // check whether this saved value is not NULL.
  char *error = dlerror();
  if (error != NULL)
    throw LibraryLoaderException("Couldn't load Symbol " + std::string(name) + ": " + std::string(dlerror()));

  return sym;
}

/** load the symbol "name" from the opened shared library "handle" */
template <typename T>
inline T LoadSymbol(Handle_t handle, const char* name) throw(LibraryLoaderException)
{
  return (T)_LoadSymbol(handle, name);
}

/**
 * @class LibraryLoader
 *
 * C++ wrapper for dynamic/run-time library loading.
 *
 * @author Boris Schauerte
 * @date 2009
 */
class LibraryLoader
{
public:
  LibraryLoader(const std::string& _libfilename,
      int _flag = RTLD_LAZY, // see man dlopen for details on flags and the loading process
      bool _autoCloseHandle = _DEFAULT_AUTO_CLOSE_HANDLE)
  : handle(NULL), flag(0), libfilename(""), autoCloseHandle(_autoCloseHandle)
  {
    OpenHandle(_libfilename, _flag);
  }
  ~LibraryLoader(void)
  {
    if (autoCloseHandle)
      CloseHandle();
  }

  template <typename T>
  inline T LoadSymbol(const char* name) throw(LibraryLoaderException) { return LoadSymbol<T>(handle, name); }
  template <typename T>
  inline T LoadSymbol(const std::string& name) throw(LibraryLoaderException) { return LoadSymbol<T>(handle, name.c_str()); }

  /* Get the library filename */
  inline const std::string& GetLibFilename(void) const { return libfilename; }
  /* Get the flag used to open the library */
  inline int GetFlag(void) const { return flag; }

  /* Should the library be automatically be closed, if the object is destroyed? */
  inline void SetAutoCloseHandle(bool _autoCloseHandle) { autoCloseHandle = _autoCloseHandle; }
  /* Should the library be automatically be closed, if the object is destroyed? */
  inline bool GetAutoCloseHandle(void) const { return autoCloseHandle; }

  /* Return the current library handle */
  inline const Handle_t GetHandle(void) const { return handle; }
  inline Handle_t GetHandle(void) { return handle; }
  /* Close the library handle */
  void CloseHandle(void) throw(LibraryLoaderException)
  {
    int error = dlclose(handle);
    if (error != 0)
      throw LibraryLoaderException("Couldn't close library: " + std::string(dlerror()));
  }
  /* Open the handle. Don't forget to close old handles. */
  void OpenHandle(const std::string& _libfilename = "", int _flag = RTLD_LAZY) throw(LibraryLoaderException)
  {
    handle = dlopen(_libfilename.c_str(), _flag);
    if (handle == NULL)
      throw LibraryLoaderException("Couldn't load library: " + std::string(dlerror()));
    else
    {
      libfilename = _libfilename;
      flag = _flag;
    }
  }
protected:
private:
  Handle_t handle;         // the handle of the loaded library
  int flag;                // the flag used to open the library
  std::string libfilename; // the filename of the loaded library
  bool autoCloseHandle;    // automatically close the handle on destruct
};

#endif // _WIN32 || WIN32

