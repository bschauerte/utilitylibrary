/**
 * Copyright 2009 B. Schauerte. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 *    1. Redistributions of source code must retain the above copyright 
 *       notice, this list of conditions and the following disclaimer.
 * 
 *    2. Redistributions in binary form must reproduce the above copyright 
 *       notice, this list of conditions and the following disclaimer in 
 *       the documentation and/or other materials provided with the 
 *       distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY B. SCHAUERTE ''AS IS'' AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL B. SCHAUERTE OR CONTRIBUTORS BE LIABLE 
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *  
 * The views and conclusions contained in the software and documentation
 * are those of the authors and should not be interpreted as representing 
 * official policies, either expressed or implied, of B. Schauerte.
 */

/** @file Cast.h
 *
 * Implemetation of special cast functions.
 *
 * @author B. Schauerte
 * @date 2009
 */
#pragma once

#include "_ul_assert.h"

#include <typeinfo>

/**
 * polymorphic_cast
 *
 * Runtime checked polymorphic downcasts and crosscasts for pointers.
 * Suggested in The C++ Programming Language, 3rd Ed, Bjarne Stroustrup,
 * section 15.8 exercise 1, page 425.
 *
 * This function matches the bevahior of dynamic_cast with references
 * and pointers, because this function also throws a std::bad_cast
 * (instead of returning a NULL pointer) if the cast went wrong.
 */
template <class T, class S>
inline T polymorphic_cast(S* x)
{
  T tmp = dynamic_cast<T>(x);
  if ( tmp == 0 )
    throw std::bad_cast();
  return tmp;
}

/**
 * polymorphic_cast
 *
 * Wrapper to dynamic cast for references.
 */
template <class T, class S>
inline T polymorphic_cast(S& x) throw( std::bad_cast )
{
  return dynamic_cast<T>(x);
}

/**
 * polymorphic_downcast
 *
 * _UL_ASSERT() checked polymorphic downcast.  Crosscasts prohibited.
 */
template <class T, class S>
inline T polymorphic_downcast(S* x)
{
  _UL_ASSERT( dynamic_cast<T>(x) == x );  // detect logic error
  return static_cast<T>(x);
}
