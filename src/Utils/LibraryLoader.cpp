/**
 * Copyright 2009 B. Schauerte. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 *    1. Redistributions of source code must retain the above copyright 
 *       notice, this list of conditions and the following disclaimer.
 * 
 *    2. Redistributions in binary form must reproduce the above copyright 
 *       notice, this list of conditions and the following disclaimer in 
 *       the documentation and/or other materials provided with the 
 *       distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY B. SCHAUERTE ''AS IS'' AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL B. SCHAUERTE OR CONTRIBUTORS BE LIABLE 
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *  
 * The views and conclusions contained in the software and documentation
 * are those of the authors and should not be interpreted as representing 
 * official policies, either expressed or implied, of B. Schauerte.
 */

#include "_ul_base_def.h"
#include "LibraryLoader.h"

//void*
//_LoadSymbol(Handle_t handle, const char* name) throw(LibraryLoaderException)
//{
//  void* sym = dlsym(handle, name);
//  // Since the value of the symbol
//  // could actually be NULL (so that a NULL return  from  dlsym()  need  not
//  // indicate  an  error),  the  correct way to test for an error is to call
//  // dlerror() to clear any old error conditions,  then  call  dlsym(),  and
//  // then call dlerror() again, saving its return value into a variable, and
//  // check whether this saved value is not NULL.
//  char *error = dlerror();
//  if (error != 0)
//    throw LibraryLoaderException("Couldn't load Symbol " + std::string(name) + ": " + std::string(dlerror()));
//
//  return sym;
//}
//
//LibraryLoader::LibraryLoader(const std::string& _libfilename, int _flag, bool _autoCloseHandle)
//: handle(NULL), flag(0), libfilename(""), autoCloseHandle(_autoCloseHandle)
//{
//  OpenHandle(_libfilename, _flag);
//}
//
//LibraryLoader::~LibraryLoader(void)
//{
//  if (autoCloseHandle)
//    CloseHandle();
//}
//
//void
//LibraryLoader::CloseHandle(void) throw(LibraryLoaderException)
//{
//  int error = dlclose(handle);
//  if (error != 0)
//    throw LibraryLoaderException("Couldn't close library: " + std::string(dlerror()));
//}
//
//void
//LibraryLoader::OpenHandle(const std::string& _libfilename, int _flag) throw(LibraryLoaderException)
//{
//  handle = dlopen(_libfilename.c_str(), _flag);
//  if (handle == NULL)
//    throw LibraryLoaderException("Couldn't load library: " + std::string(dlerror()));
//  else
//  {
//    libfilename = _libfilename;
//    flag = _flag;
//  }
//}
