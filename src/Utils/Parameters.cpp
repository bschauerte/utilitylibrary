/**
 * Copyright 2009 B. Schauerte. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 *    1. Redistributions of source code must retain the above copyright 
 *       notice, this list of conditions and the following disclaimer.
 * 
 *    2. Redistributions in binary form must reproduce the above copyright 
 *       notice, this list of conditions and the following disclaimer in 
 *       the documentation and/or other materials provided with the 
 *       distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY B. SCHAUERTE ''AS IS'' AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL B. SCHAUERTE OR CONTRIBUTORS BE LIABLE 
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *  
 * The views and conclusions contained in the software and documentation
 * are those of the authors and should not be interpreted as representing 
 * official policies, either expressed or implied, of B. Schauerte.
 */

#include "_ul_base_def.h"
#include "Parameters.h"

#include <fstream>
#include <iostream>

//#include "boost/archive/xml_oarchive.hpp"
//#include "boost/archive/xml_iarchive.hpp"
//#include "boost/archive/binary_oarchive.hpp"
//#include "boost/archive/binary_iarchive.hpp"

#include "boost/archive/archive_exception.hpp" 

#include "boost/serialization/version.hpp"
#include "boost/serialization/export.hpp"

std::string Parameters::parametersDirectory = "Parameters";

BOOST_CLASS_EXPORT_GUID(Parameters, "Parameters")
BOOST_CLASS_VERSION(Parameters,1)

BOOST_CLASS_EXPORT_GUID(ExampleParameters, "ExampleParameters")
BOOST_CLASS_VERSION(ExampleParameters,1)

Parameters::Parameters(void)
: saveChanges(false), parametersName("Parameters")
{
}

Parameters::Parameters(const std::string name) 
: saveChanges(false), parametersName(name), ownerId("Unknown")
{
}
Parameters::~Parameters(void)
{
}

//void Parameters::Load(const std::string filename, Parameters* params)
//{
//  std::string configname = parametersDirectory + "/" + filename + ".xml";
//  if (!ReadFromFileXML(configname, params))
//    // Write config, if the config does not exist atm.
//    if(!WriteToFileXML(configname, params))
//      std::cerr << "Could not load or create parameters file " << configname << std::endl;
//}
//
//void Parameters::Load(const std::string filename)
//{
//  std::string configname = parametersDirectory + "/" + filename + ".xml";
//  if (!ReadFromFileXML(configname))
//    // Write config, if the config does not exist atm.
//    if(!WriteToFileXML(configname))
//      std::cerr << "Could not load or create parameters file " << configname << std::endl;
//}

void Parameters::Save(const std::string filename) const
{
  std::string configname;
  if (filename.c_str()[0] == '#') // override default parameters directory
  {
    configname = filename + ".xml";
    configname.erase(0,1);
  }
  else
    configname = parametersDirectory + "/" + filename + ".xml";
  if(!WriteToFileXML(configname))
  {
#ifdef USE_LOGMESSAGE
    LogMessage lm("Utils.Parameters.Save", LogMessage::LOG_ERROR);
    lm.msg_stream() << "Could not save parameters file " << configname;
    std::cerr << lm << std::endl;
#else
    std::cerr << "Could not save parameters file " << configname << std::endl;
#endif
  }
}

void Parameters::Save(const std::string filename, const Parameters* params)
{
  std::string configname;
  if (filename.c_str()[0] == '#') // override default parameters directory
  {
    configname = filename + ".xml";
    configname.erase(0,1);
  }
  else
    configname = parametersDirectory + "/" + filename + ".xml";
  if(!WriteToFileXML(configname, params))
  {
#ifdef USE_LOGMESSAGE
    LogMessage lm("Utils.Parameters.Save", LogMessage::LOG_ERROR);
    lm.msg_stream() << "Could not save parameters file " << configname;
    std::cerr << lm << std::endl;
#else
    std::cerr << "Could not save parameters file " << configname << std::endl;
#endif  
  }
}

bool Parameters::WriteToFileXML(std::string filename, const Parameters* params)
{
  std::ofstream ofs(filename.c_str());
  //_UL_ASSERT(ofs.good());
  if (ofs)
  {
    try
    {
      boost::archive::xml_oarchive oa(ofs);
      oa << boost::serialization::make_nvp(params->parametersName.c_str(), params);
      ofs.close();
    }
    catch (boost::archive::archive_exception e)
    {
      std::cerr << "Parameters::ReadFromFileXML(" << filename << ") - Serialization ERROR" << std::endl;
      std::cerr << "Archive exception: " << e.what() << std::endl;
      exit(-1); // we can not ignore/recover bad configs -> quit
    }
    catch (std::exception e)
    {
      std::cerr << "Parameters::ReadFromFileXML(" << filename << ") - Serialization ERROR" << std::endl;
      std::cerr << "Exception: " << e.what() << std::endl;
      exit(-1); // we can not ignore/recover bad configs -> quit
    }
    return true;
  }
  else
  {
    return false;
  }
}

bool Parameters::WriteToFileXML(std::string filename) const
{
  return WriteToFileXML(filename, this);
}

Parameters* Parameters::ReadFromFileXML(std::string filename)
{
  std::ifstream ifs(filename.c_str());
  if (ifs.good())
  {
    Parameters* params = NULL;
    try
    {
      boost::archive::xml_iarchive ia(ifs);
      params = new Parameters();  // tmp parameters -> for params->parametersName.c_str() ... 
        //@todo: ^^ bedeutet aber auch das bei make_nvp params->parametersName.c_str() eigentlich nicht beachtet wird, weil das nur Params ist und nicht der ParametersName der richtigen Subklassen
      Parameters* tmp = params;    
      ia >> boost::serialization::make_nvp(params->parametersName.c_str(), params);
      delete tmp; // params points now to the new created parameters, and tmp to the previously created temporary Parameters-Pointer
      ifs.close();
    }
    catch (boost::archive::archive_exception e)
    {
      std::cerr << "Parameters::ReadFromFileXML(" << filename << ") - Serialization ERROR" << std::endl;
      std::cerr << "Archive exception: " << e.what() << std::endl;
      exit(-1); // we can not ignore/recover bad configs -> quit
    }
    catch (std::exception e)
    {
      std::cerr << "Parameters::ReadFromFileXML(" << filename << ") - Serialization ERROR" << std::endl;
      std::cerr << "Exception: " << e.what() << std::endl;
      exit(-1); // we can not ignore/recover bad configs -> quit
    }
    return params;
  }
  else
  {
    return NULL;
  }
}

Parameters* Parameters::ReadFromFileXML(std::string filename, const std::string& parametersName)
{
  std::ifstream ifs(filename.c_str());
  if (ifs.good())
  {
    Parameters* params = NULL;
    try
    {
      boost::archive::xml_iarchive ia(ifs);
      ia >> boost::serialization::make_nvp(parametersName.c_str(), params); // @todo: parametersName wirklich notw.? siehe Beobachtung weiter oben
        // @todo: mgliche gute Lsung -> ersetze berall die Verwendung von parametersName durch Konstante "Params" oder hnliches!
      ifs.close();
    }
    catch (boost::archive::archive_exception e)
    {
      std::cerr << "Parameters::ReadFromFileXML(" << filename << ") - Serialization ERROR" << std::endl;
      std::cerr << "Archive exception: " << e.what() << std::endl;
      exit(-1); // we can not ignore/recover bad configs -> quit
    }
    catch (std::exception e)
    {
      std::cerr << "Parameters::ReadFromFileXML(" << filename << ") - Serialization ERROR" << std::endl;
      std::cerr << "Exception: " << e.what() << std::endl;
      exit(-1); // we can not ignore/recover bad configs -> quit
    }
    return params;
  }
  else
  {
    return NULL;
  }
}
