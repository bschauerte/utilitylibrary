/**
 * Copyright 2009 B. Schauerte. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 *    1. Redistributions of source code must retain the above copyright 
 *       notice, this list of conditions and the following disclaimer.
 * 
 *    2. Redistributions in binary form must reproduce the above copyright 
 *       notice, this list of conditions and the following disclaimer in 
 *       the documentation and/or other materials provided with the 
 *       distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY B. SCHAUERTE ''AS IS'' AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL B. SCHAUERTE OR CONTRIBUTORS BE LIABLE 
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *  
 * The views and conclusions contained in the software and documentation
 * are those of the authors and should not be interpreted as representing 
 * official policies, either expressed or implied, of B. Schauerte.
 */

#pragma once

#include "_ul_base_def.h"

#include <map>
#include <fstream>
#include <iostream>
#include <list>

#include "boost/archive/binary_oarchive.hpp"
#include "boost/archive/binary_iarchive.hpp"
#include "boost/archive/xml_oarchive.hpp"
#include "boost/archive/xml_iarchive.hpp"
#include "boost/serialization/serialization.hpp"
#include "boost/lexical_cast.hpp"
#include "boost/cast.hpp"

#include "Utils/Registration.h"
#include "Utils/ToString.h"
#ifdef USE_LOGMESSAGE
#include "Utils/Log/LogMessage.h"
#endif
#include "Utils/Assert.h"

/* Macro definitions for the Parameters::Set method. */
#define _READ_MAP_PARAM_INIT()         std::map<std::string, std::string>::iterator __it;
#define _READ_MAP_PARAM_INIT_SUPER()   Parameters::Set(parameters); std::map<std::string, std::string>::iterator __it;
#define _READ_MAP_PARAM(name, type)    __it = parameters.find( #name ); \
                                        if (__it != parameters.end()) \
                                        name = boost::lexical_cast<type>(__it->second);
#define _READ_MAP_PARAM_NAMED(varname, name, type)    __it = parameters.find( name ); \
                                                      if (__it != parameters.end()) \
                                                      varname = boost::lexical_cast<type>(__it->second);
#define _READ_MAP_PARAM_STRING(name)   __it = parameters.find( #name ); \
                                        if (__it != parameters.end()) \
                                        name = __it->second;
#define _READ_MAP_PARAM_ARRAY(size, arr_name, type) for (int __i = 0; __i < size; __i++) \
                                        { \
                                          std::string name = std::string(#arr_name "[") + ToString(__i) + std::string("]"); \
                                          __it = parameters.find( name ); \
                                          if (__it != parameters.end()) \
                                            arr_name[__i] = boost::lexical_cast<type>(__it->second); \
                                        }
#define _READ_MAP_PARAM_END()
/* Macro definitions for the Parameters::Get method. */
#define _WRITE_MAP_PARAM_INIT()       bool wasEmpty = parameters.empty(); \
                                        std::map<std::string, std::string>::iterator __it;
#define _WRITE_MAP_PARAM_INIT_SUPER() bool wasEmpty = parameters.empty(); Parameters::Get(parameters); \
                                        std::map<std::string, std::string>::iterator __it;
#define _WRITE_MAP_PARAM(name)        __it = parameters.find( #name ); \
                                      if (__it != parameters.end() || wasEmpty == true) \
                                      parameters[ #name ] = ToString( name );
#define _WRITE_MAP_PARAM_NAMED(varname, name)  __it = parameters.find( name ); \
                                               if (__it != parameters.end() || wasEmpty == true) \
                                               parameters[ name ] = ToString( varname );
#define _WRITE_MAP_PARAM_ARRAY(size, arr_name)  for (int __i = 0; __i < size; __i++) \
                                        { \
                                          std::string name = std::string(#arr_name "[") + ToString(__i) + std::string("]"); \
                                          __it = parameters.find(name); \
                                          if (__it != parameters.end() || wasEmpty == true) \
                                            parameters[name] = ToString( arr_name[__i] ); \
                                        }
#define _WRITE_MAP_PARAM_END()

/**
 * @class Parameters
 *
 * Base class for parameter-classes.
 * See ExampleParameters-class as usage example.
 * Don't forget BOOST_CLASS_EXPORT_GUID.
 *
 * @author Boris Schauerte
 * @date 2009
 */
class Parameters
{
public:
  Parameters(void);
  // Constructor should be used to define default-values
  Parameters(const std::string name);
  // virtual necessary, because only if Parameters-class is virtual, then the
  // polymorphy works
  virtual ~Parameters(void);

  /** Standard method to Load parameters.
   * @param filename   filename of the configuration that should be loaded
   *                   note: - do NOT specify a file ending!
   *                         - convention for modules: moduleTypeName + "." + moduleName
   *                         - config file will be created if it doesn't exist
   */
  template <typename T>
  static T* Load(const std::string& filename, const std::string& parametersName, bool autoCreate = false)
  {
    std::string configname;
    if (filename.c_str()[0] == '#') // override default parameters directory
    {
      configname = filename + ".xml";
      configname.erase(0,1);
    }
    else
      configname = parametersDirectory + "/" + filename + ".xml";
    Parameters* tmp = ReadFromFileXML(configname, parametersName);
    if (!tmp)
    {
#ifdef USE_LOGMESSAGE
      LogMessage lm("Utils.Parameters.Load", LogMessage::LOG_ERROR);
      lm.msg_stream() << "Could not load parameters file " << configname;
      std::cerr << lm << std::endl;
#else
      std::cerr << "Could not load parameters file " << configname << std::endl;
#endif
      if (autoCreate)
      {
        tmp = new T();
#ifdef USE_LOGMESSAGE
        lm.level() = LogMessage::LogLevelToString(LogMessage::LOG_INFO);
        lm.msg(""); // reset
        lm.msg_stream() << "Trying to create parameters file " << configname;
        std::cout << lm << std::endl;
#else
        std::cout << "Trying to create parameters file " << configname << std::endl;
#endif
        if(!tmp->WriteToFileXML(configname))
        {
#ifdef USE_LOGMESSAGE
          lm.level() = LogMessage::LogLevelToString(LogMessage::LOG_ERROR);
          lm.msg(""); // reset
          lm.msg_stream() << "Could not create parameters file " << configname;
          std::cerr << lm << std::endl;
#else
          std::cout << "Could not create parameters file " << configname << std::endl;
#endif
        }
      }
    }
    return boost::polymorphic_downcast<T*>(tmp);
  }

  /** Standard method to Save parameters.
   * @param filename   specifies the filename where the configuration will be saved
   *                   note: - do NOT specify a file ending!
   *                         - convention for modules: moduleTypeName + "." + moduleName
   */
  void Save(const std::string filename) const;
  static void Save(const std::string filename, const Parameters* params);

  /** Write parameters to a XML file
   * @param filename The name of the xml-file
   * @param params   The parameters to write
   * @return         true if file could be written, false otherwise
   */
  static bool WriteToFileXML(std::string filename, const Parameters* params);

  /** Write parameters to a XML file
   * @param filename The name of the xml-file
   * @return         true if file could be written, false otherwise
   */
  bool WriteToFileXML(std::string filename) const;

  /** Read parameters from a XML file
   * @param filename The name of the xml-file
   * @param params   The parameters to read
   * @return         true if file could be read, false otherwise
   */
  // this is the more inefficient version
  static Parameters* ReadFromFileXML(std::string filename);
  /** Read parameters from a XML file
   * @param filename The name of the xml-file
   * @param params   The parameters to read
   * @return         true if file could be read, false otherwise
   */
  // more efficient, but requires parametersName for the NVP (a bit unconvenient, but ...)
  static Parameters* ReadFromFileXML(std::string filename, const std::string& parametersName);

  /** Set parameters using the name-value pairs from a map.
   * This has the advantage, that only a subset of parameter variables can be set
   * (this means, if the name(-value pair) of a variable is not in the map, than the
   *  value of the variable won't be changed)
   * Note:
   *   Set is not optimized for speed (e.g. iterators where not used)
   * @param parameters A map containing the parameters as name-value pairs
   */
  virtual void Set(std::map<std::string, std::string>& parameters)
  {
    if (!parameters.empty())
    {
      _READ_MAP_PARAM_INIT();
      _READ_MAP_PARAM_STRING(parametersName);
      _READ_MAP_PARAM(saveChanges,bool);
      _READ_MAP_PARAM_END();
    }
  }

  /** Get parameters as name-value pairs from a map.
   * Note:
   *   Get is not optimized for speed (e.g. iterators where not used)
   * @param parameters The map will contain the parameters as name-value pairs.
   *                   If the map already contains name-value pairs, then only the values of contained names will be updated.
   *                   If the map is empty, then all name-value pairs are stored in parameters.
   */
  virtual void Get(std::map<std::string, std::string>& parameters)
  {
    // add values of the variables with name in parameters
    _WRITE_MAP_PARAM_INIT();
    _WRITE_MAP_PARAM(parametersName);
    _WRITE_MAP_PARAM(saveChanges);
    _WRITE_MAP_PARAM_END();
  }

  /** Declare friend-class, necessary for boost::serialization */
  friend class boost::serialization::access;
  /** The serialize method. Main method for in-/output of parameters. */
  template <class Archive>
  void serialize(Archive & ar, const unsigned int version)
  {
    ar & BOOST_SERIALIZATION_NVP(parametersName)
       & BOOST_SERIALIZATION_NVP(saveChanges);
  }

  //void SetParametersName(const std::string& name) { parametersName = name; }
  /** Get the name of the parameters-class. */
  std::string GetParametersName(void) const { return parametersName; }
  /** Get the information if the changes should be saved. */
  bool GetSaveChanges(void) const { return saveChanges; }

  /** Set the OwnerID. */
  void SetOwnerId(const std::string& id) { ownerId = id; }
  /** Get the OwnerID. */
  std::string GetOwnerId(void) const { return ownerId; }

  /** Global parametersDirectory setting. Can be used but doesn't have to. */
  static std::string parametersDirectory;
protected:
  bool saveChanges;             // Should (runtime-)changes to the parameters be saved?
  std::string parametersName;   // Name of the parameters-class
  std::string ownerId;          // Id of the owner of the parameters
private:
};


//
// Example
//   simple example of Parameters-class.
//
/**
 * @class ExampleParameters
 *
 * Example usage of the Parameter-class as base-class.
 *
 * @author Boris Schauerte
 * @date 2009
 */
class ExampleParameters
  : public Parameters
{
public:
  ExampleParameters()
    : Parameters("ExampleParameters"), exampleString("exampleParameters")
  {
  }
  virtual ~ExampleParameters() {}

  virtual void Set(std::map<std::string, std::string>& parameters)
  {
    if (!parameters.empty())
    {
      Parameters::Set(parameters);
      _READ_MAP_PARAM_INIT();
      _READ_MAP_PARAM_STRING(exampleString);
    }
  }

  friend class boost::serialization::access;
  template <class Archive>
  void serialize(Archive & ar, const unsigned int version)
  {
    ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(Parameters)
       & BOOST_SERIALIZATION_NVP(exampleString);
    boost::serialization::void_cast_register<ExampleParameters,Parameters>();
  }
protected:
private:
  std::string exampleString;
};
