/**
 * Copyright 2009 B. Schauerte. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 *    1. Redistributions of source code must retain the above copyright 
 *       notice, this list of conditions and the following disclaimer.
 * 
 *    2. Redistributions in binary form must reproduce the above copyright 
 *       notice, this list of conditions and the following disclaimer in 
 *       the documentation and/or other materials provided with the 
 *       distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY B. SCHAUERTE ''AS IS'' AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL B. SCHAUERTE OR CONTRIBUTORS BE LIABLE 
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *  
 * The views and conclusions contained in the software and documentation
 * are those of the authors and should not be interpreted as representing 
 * official policies, either expressed or implied, of B. Schauerte.
 */

#pragma once

/** @file AllocMatrix.h
 *
 * Wrappers to simplify allocation/release of matrices (with a compact memory layout).
 *
 * @author B. Schauerte
 * @date 2009
 */

#include <stdlib.h> // malloc

/**
 * Allocate memory for a 2D <T> matrix of size [row,col].
 * This returns a vector of pointers to the rows of the matrix.
 * The memory is allocated as block (i.e. a single rows*cols array), thus
 * algorithm for 1D arrays can operate on the data as well. The pointer
 * to the whole data block is the pointer of the 1st row.
 */
template <typename T, typename S>
inline T**
AllocMatrix(const S rows, const S cols)
{
  S i;
  T **m, *v;

  m = (T **) malloc(rows * sizeof(T *));
  v = (T *) malloc(rows * cols * sizeof(T));
  for (i = 0; i < rows; i++)
  {
    m[i] = v;
    v += cols;
  }

  return (m);
}

/** Free memory allocated by AllocMatrix */
template <typename T>
inline void
FreeMatrix(T**& m)
{
  if (m[0] != NULL)
    free((void *) m[0]);
  if (m != NULL)
    free((void *) m);
  m = NULL;
}

