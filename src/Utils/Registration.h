/**
 * Copyright 2009 B. Schauerte. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 *    1. Redistributions of source code must retain the above copyright 
 *       notice, this list of conditions and the following disclaimer.
 * 
 *    2. Redistributions in binary form must reproduce the above copyright 
 *       notice, this list of conditions and the following disclaimer in 
 *       the documentation and/or other materials provided with the 
 *       distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY B. SCHAUERTE ''AS IS'' AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL B. SCHAUERTE OR CONTRIBUTORS BE LIABLE 
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *  
 * The views and conclusions contained in the software and documentation
 * are those of the authors and should not be interpreted as representing 
 * official policies, either expressed or implied, of B. Schauerte.
 */

#pragma once

#include "_ul_base_def.h"

#include <vector>
#include <algorithm>

#define _T_REGISTER( type , item )   Registration<type>::GetInstance()->Register(item);
#define _T_UNREGISTER( type , item ) Registration<type>::GetInstance()->Unregister(item);
#define _T_REGISTRATION( type )      (*Registration<type>::GetInstance())

/**
 * @class Registration
 *
 * Template class to provide GLOBAL access to registered objects (e.g. Parameters).
 * This class is realized as singleton.
 *
 * Notes:
 * - this implementation is not thread-safe
 * - don't forget to add instantiation in .cpp-file if needed
 *
 * @author Boris Schauerte
 * @date 2009
 */
template <class T>
class Registration
{
public:
  /** Get a pointer to the (singleton) instance of the Registration. */
  static Registration<T>* GetInstance(void)
  {
    return registration;
  }
  /** Get a reference to the (singleton) instance of the Registration. */
  static Registration<T>& GetReference(void)
  {
    return (*registration);
  }

  /** Register item. */
  void Register(const T item)
  {
    registeredItems.push_back(item);
  }
  /** Unregister item. */
  void Unregister(const T item)
  {
    remove(item);
  }

  /** Get number of registered items. */
  size_t GetSize(void) const { return registeredItems.size(); }

  /** Array-access to registered items. */
  T& operator[](int index)
  {
    return registeredItems.at(index);
  }
  /** Array-access to registered items. */
  T& operator[](size_t index)
  {
    return registeredItems.at(index);
  }

protected:
  /* removes all elements that are equal to item from registeredItems */
  void remove(T item)
  {
    while (true)
    {
      typename std::vector<T>::iterator it;
      it = std::find(registeredItems.begin(),registeredItems.end(),item);
      if (it == registeredItems.end()) // not found
        break;
      registeredItems.erase(it);
    }
  }
private:
  /* Singleton implementation */
  Registration(void) { }
  ~Registration(void) { }
  static Registration<T>* registration;

  std::vector<T> registeredItems; // vector that contains all registered items
};
