#pragma once

#include "_ul_base_def.h"

#include <string>
#include <stdexcept>
#include <sstream>
#include <iostream>

using namespace std;

/**
 * @class LogMessage
 *
 * Standardize the log messages.
 * Makes output cleaner and is a step toward a unified log message handling
 * (e.g. various processes connected to a single logserver).
 * 
 * @author Boris Schauerte
 */
class LogMessage
{
public:
  enum LogLevel
  {
    LOG_INFO,
    LOG_WARNING,
    LOG_EXCEPTION,
    LOG_BENCHMARK,
    LOG_ERROR
  };
  static string LogLevelToString(const LogLevel& lvl);

  LogMessage(const string& module, const string& level, const std::string& msg = "");
  LogMessage(const string& module, const LogLevel& level, const std::string& msg = "");
  ~LogMessage(void);

  /* Return the LogMessage as string. */
  std::string str(void) const;
  /* Get/set the level. */
  std::string& level(void);
  /* Get/set the module. */
  std::string& module(void);
  /* Get the stream. */
  ostream& msg_stream(void);
  /* Get the current message. */
  string msg(void) const;
  /* Set the message. */
  void msg(const string& _msg);
  /* Print message to ostream os (by default std::cout) */
  void print(ostream& os = std::cout) const;
  /* Print message to ostream os (by default std::cout) with additional line end ('\n', std::endl) */
  void println(ostream& os = std::cout) const;
  // ... a couple of specializations of print/println for handier use ...
  /* Print message to std::cout */
  void cout(void) const;
  /* Print message to std::cout with additional line end ('\n', std::endl) */
  void coutln(void) const;
  /* Print message to std::cerr */
  void cerr(void) const;
  /* Print message to std::cerr with additional line end ('\n', std::endl) */
  void cerrln(void) const;

  /* Reset all informations */
  ostream& reset(const string& module_, const LogLevel& level_, const std::string& msg_ = "");
  /* Reset level and msg, this will be the most common case */
  ostream& reset(const LogLevel& level_, const std::string& msg_ = "");
  /* Reset only the text message. The difference between this and msg(...) is
   * that Reset returns the ostream and therefore simplifies the message creation, e.g.
   * logMessage.reset("constant text foo") << variableBar;
   */
  ostream& reset(const std::string& _msg = "");

  friend ostream& operator <<(ostream &os,const LogMessage &obj);
protected:
private:
  //string _program;
  string _module;
  string _level;
  //string _msg;
  std::ostringstream _msg_stream;
};

/* Write LogMessage to a stream. */
ostream& operator <<(ostream &os,const LogMessage &lm);
