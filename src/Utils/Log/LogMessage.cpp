#include "_ul_base_def.h"
#include "LogMessage.h"

string 
LogMessage::LogLevelToString(const LogLevel& lvl) 
{
  switch (lvl)
  {
  case LOG_INFO:
    return "INFO";
    break;
  case LOG_WARNING:
    return "WARNING";
    break;
  case LOG_EXCEPTION:
    return "EXCEPTION";
    break;
  case LOG_ERROR:
    return "ERROR";
    break;
  case LOG_BENCHMARK:
    return "BENCHMARK";
    break;
  default:
    throw std::invalid_argument(LogMessage("Utils.LogMessage", LOG_EXCEPTION, "Invalid level").str());
  }
}

LogMessage::LogMessage(const string& module, 
                       const string& level, 
                       const string& msg)
: _module(module), _level(level)
{
  _msg_stream << msg;
}

LogMessage::LogMessage(const string& module, 
                       const LogLevel& level, 
                       const string& msg)
: _module(module), _level(LogLevelToString(level))
{
  _msg_stream << msg;
}

LogMessage::~LogMessage(void)
{
}

std::string 
LogMessage::str(void) const
{
  std::ostringstream os;
  os << *this;
  return os.str();
}

std::string& 
LogMessage::level(void)
{
  return _level;
}

std::string& 
LogMessage::module(void)
{
  return _module;
}

ostream& 
LogMessage::msg_stream(void)
{
  return _msg_stream;
}

string 
LogMessage::msg(void) const
{
  return _msg_stream.str();
}

void 
LogMessage::msg(const string& _msg)
{
  _msg_stream.str("");
  _msg_stream << _msg;
  //_msg_stream.str(_msg);
}

void 
LogMessage::print(ostream &os) const
{
  os << *this;
}

void 
LogMessage::println(ostream &os) const
{
  os << *this << std::endl;
}

void 
LogMessage::cout(void) const
{
  print();
}

void 
LogMessage::coutln(void) const
{
  println();
}

void 
LogMessage::cerr(void) const
{
  print(std::cerr);
}

void 
LogMessage::cerrln(void) const
{
  println(std::cerr);
}

//ostream& 
//LogMessage::reset(const string& module_, const string& level_, const std::string& msg_)
//{
//  _module = module_;
//  _level = level_;
//  msg(msg_);
//  return msg_stream();
//}

ostream& 
LogMessage::reset(const string& module_, const LogLevel& level_, const std::string& msg_)
{
  _module = module_;
  _level = LogLevelToString(level_);
  msg(msg_);
  return msg_stream();
}

//ostream& 
//LogMessage::reset(const string& level_, const std::string& msg_)
//{
//  _level = level_;
//  msg(msg_);
//  return msg_stream();
//}

ostream& 
LogMessage::reset(const LogLevel& level_, const std::string& msg_)
{
  _level = LogLevelToString(level_);
  msg(msg_);
  return msg_stream();
}


ostream& 
LogMessage::reset(const std::string& msg_)
{
  msg(msg_);
  return msg_stream();
}

ostream& operator <<(ostream &os,const LogMessage &lm)
{
  os << "(" << lm._level << ")" 
    << lm._module << ": " 
    << lm.msg();
  return os;
}

