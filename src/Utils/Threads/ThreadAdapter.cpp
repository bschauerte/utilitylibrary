#include "_ul_base_def.h"
#include "ThreadAdapter.h"

ThreadAdapter::ThreadAdapter(void (*func)(void*), void* param) 
  : _func(func), _param(param) 
{
}

ThreadAdapter::~ThreadAdapter(void)
{
}

void ThreadAdapter::operator()() const
{ 
  _func(_param); 
}
