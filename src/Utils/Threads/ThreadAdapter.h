#pragma once

#include "_ul_base_def.h"

/**
 * @class ThreadAdapter
 *
 * Thread adapter, from functions to classes which are supported
 * by boost::thread. This is not necessary, but can make the implementation
 * more flexible.
 *
 * @author Boris Schauerte
 */
class ThreadAdapter
{
public:
  ThreadAdapter(void (*func)(void*), void* param);
  ~ThreadAdapter(void);
  void operator()() const;

protected:
  void (*_func)(void*);
  void* _param;
};
