#include "_ul_base_def.h"
#include "Threader.h"

#include "boost/foreach.hpp"

#include "_ul_assert.h"

Threader::Threader(void)
  : runThat(NULL), countRunnable(0)
{
}

Threader::Threader(Threader::Runnable* that)
  : runThat(that), countRunnable(0)
{
  _UL_ASSERT(that != NULL);

  if (that)
    countRunnable++;
}

Threader::~Threader(void)
{
}

void Threader::operator()()
{
  switch(countRunnable)
  {
  case 0:
    // no object to be executed, do nothing
    break;
  case 1:
    runThat->SetIsRunning(true);
    runThat->Run();
    runThat->SetIsRunning(false);
    break;
  default:
    BOOST_FOREACH( Runnable* toRun , runThese )
    {
      toRun->SetIsRunning(true);
      toRun->Run();
      toRun->SetIsRunning(false);
    }
    break;
  }

  //Alternative code - faster if it is assumed that countRunnable=0 never happens
  //if (runThat)
  //{
  //  runThat->Run();
  //}
  //else
  //{
  //  BOOST_FOREACH(Threadable* toRun , runThese)
  //  {
  //    toRun->Run();
  //  }
  //}
}

void Threader::Add(Threader::Runnable* toRun)
{
  _UL_ASSERT(toRun != NULL);

  if (toRun != NULL)
  {
    switch(countRunnable)
    {
    case 0:
      runThat = toRun;
      break;
    case 1:
      runThese.push_back(runThat);
      runThat = NULL;
    default:
      runThese.push_back(toRun);
      break;
    }
    countRunnable++;
  }
}

Threader::t_thread* Threader::StartAsThread(Threader *threader)
{
  return new t_thread(*threader);
}

Threader::t_thread* Threader::StartAsThread(void)
{
  return new t_thread(*this);
}
