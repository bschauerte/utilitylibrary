#pragma once

#include "_ul_base_def.h"

#include "boost/thread/condition.hpp"
#include "boost/thread/thread.hpp"
#include "boost/thread/xtime.hpp"

/**
 * @class DataSync
 *
 * Class to simplify asnchronous/threaded producer/consumer pattern.
 * Implemented as "noncopyable", so users have to share via pointers or
 * references.
 *
 * @todo test with multiple consumers, should work but is untested!
 *
 * @author Boris Schauerte
 */
class DataSync
  : private boost::noncopyable
{
public:
  DataSync(void);
  ~DataSync(void);

  /** Notify one consumer that new data arrived. */
  inline void NotifyOne(void)
  {
    waitCond.notify_one();
  }
  /** Notify all comsumer that new data arrived. */
  inline void NotifyAll(void)
  {
    waitCond.notify_all();
  }
  /** Wait for new data. */
  inline void Wait(void)
  {
    boost::mutex::scoped_lock lk(monitor);
    waitCond.wait(lk);
  }

  /** Wait for new data, but only a maximum duration. */
  inline void Wait(unsigned int ms)
  {
    boost::xtime xt;
    boost::xtime_get(&xt, boost::TIME_UTC);
    xt.nsec += ms * 1000000;
    boost::mutex::scoped_lock lk(monitor);
    waitCond.timed_wait(lk, xt);
  }
protected:
private:
  boost::condition waitCond;
  boost::mutex monitor;
};
