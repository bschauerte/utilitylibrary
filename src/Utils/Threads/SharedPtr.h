/**
 * @class SharedPtr
 *
 * A simple "thread-safe" shared pointer implementation.
 * Please note, it is not possible to write an entirely safe implementation, because there
 * are some release/acquire sequences that can not be sufficiently handled with a counter
 * (e.g. [due to scheduling] releasing a unique element just a time instance before acquiring
 * a copy of that element!). Ensure -- by design -- that these cases never happen - otherwise
 * you are seriously fucked!
 *
 * WARNING - NOT THOROUGHLY TESTED!
 *
 * @author Boris Schauerte
 * @author (unknown; http://ootips.org/yonat/4dev/counted_ptr.h)
 */
/*
 * - The is a non-intrusive implementation that allocates an additional int and pointer for every counted object.
 * - (mostly) STL compatible
 * - inspired by boost::shared_ptr
 */
#pragma once

#include "_ul_base_def.h"

#include "boost/thread/thread.hpp"

template <class X>
class SharedPtr
{
public:
    typedef X element_type;

    explicit SharedPtr(X* p = 0) // allocate a new counter
    : itsCounter(0)
    {
      if (p)
        itsCounter = new __Counter(p);
    }
    SharedPtr(const SharedPtr& r) throw()
    {
      acquire(r.itsCounter);
    }
    ~SharedPtr()
    {
      release();
    }

    SharedPtr& operator=(const SharedPtr& r)
    {
      if (this != &r)
      {
        release();
        acquire(r.itsCounter);
      }
      return *this;
    }

    X& operator*(void)  const { return *itsCounter->ptr; }
    X* operator->(void) const { return itsCounter->ptr; }
    X* get(void)        const { return itsCounter ? itsCounter->ptr : 0; }

    bool unique(void)   const { return (itsCounter ? itsCounter->count == 1 : true); }
    unsigned int use_count(void) const { return (itsCounter ? itsCounter->count : 0); }

private:

    struct __Counter
    {
      __Counter(X* p = 0, unsigned c = 1)
      : ptr(p), count(c)
      {
      }
      ~__Counter(void)
      {
      }

      X*           ptr;
      unsigned int count;
      boost::mutex monitor;
    };
    __Counter* itsCounter;

    void acquire(__Counter* c) throw()
    {
      // increment the count
      itsCounter = c;
      if (c)
      {
        boost::mutex::scoped_lock lk(itsCounter->monitor);
        ++c->count;
      }
    }

    void release()
    {
      // decrement the count, delete if it is 0
      if (itsCounter)
      {
        boost::mutex::scoped_lock lk(itsCounter->monitor);
        if (--itsCounter->count == 0)
        {
          delete itsCounter->ptr;
          delete itsCounter;
        }
        itsCounter = 0;
      }
    }
};

