#pragma once

#include "_ul_base_def.h"

#include <vector>

#include "boost/thread/thread.hpp"

#include "Utils/xsleep.h"

/**
 * @class Threader
 *
 * Helper class to solve problems with boost::threads and virtual member functions.
 *
 * @author Boris Schauerte
 */
class Threader
{
public:
  typedef boost::thread t_thread;

  /**
   * Interface class for Threader-class.
   */
  class Runnable
  {
    public:
      Runnable(void) : isRunning(false) {}
      virtual ~Runnable(void) {}

      /* All Threader::Runnable objects need to implement this method, this
       * method will be executed by the Threader.
       */
      virtual void Run() = 0;

      /* Is this Runnable object currently running? */
      bool IsRunning(void) const { return isRunning; }
      /* Set the isRunning state. */
      void SetIsRunning(bool running) { isRunning = running; }

      static void Sleep(unsigned int ms) { xsleep(ms); }
    protected:
      bool isRunning;
    private:
  };

  Threader(void);
  Threader(Threader::Runnable* that);
  ~Threader(void);

  /* Interface to boost::threads */
  void operator()();
  /* Add another Runnable object that should be run in this thread. */
  void Add(Threader::Runnable* toRun);
  /* Start this Threader as a new thread. Please note that the returned
   * boost::thread has to be deleted.
   */
  static t_thread* StartAsThread(Threader* threader);
  t_thread* StartAsThread(void);
  size_t GetCountRunnable(void) const { return countRunnable; }

protected:
  /* Holds the runnable object, if there is exactly one */
  Threader::Runnable* runThat;
  /* Holds the runnable objects, if there is more than one runnable object */
  std::vector<Threader::Runnable*> runThese;
  /* Amount of runnable objects in this Threader */
  size_t countRunnable;
private:
};
