#pragma once

#include "_ul_base_def.h"

#include <stdexcept>
#include <queue>

#include "boost/thread/thread.hpp"
#include "boost/thread/mutex.hpp"
#include "DataSync.h"

#if (defined _WIN32 || defined WIN32) && defined GetMessage
#undef GetMessage
#endif

/**
* @class MessageQueueException
*
* Base class for exceptions thrown by MessageQueue.
*
* @author Boris Schauerte
*/
class MessageQueueException 
  : public std::exception 
{
};
/**
* @class EmptyMessageQueueException
*
* Exception indicates an unappropriate use of an empty MessageQueue.
*
* @author Boris Schauerte
*/
class EmptyMessageQueueException 
  : public MessageQueueException 
{
public: 
  virtual const char* what() const throw()
  {
    return "MessageQueue is empty";
  }
};
/**
* @class FullMessageQueueException
*
* Exception indicates an unappropriate use of a full MessageQueue.
*
* @author Boris Schauerte
*/
class FullMessageQueueException 
  : public MessageQueueException 
{
public: 
  virtual const char* what() const throw()
  {
    return "MessageQueue is full";
  }
};

/**
* @class MessageQueue
*
* Implements a (simple) thread-safe queue for data transfer between threads. Due
* to performance issue it is recommended to use this class with pointers only (or
* very simple messages), because the assignment-operator is used.
*
* @todo implement allow multiple-reader threads at the same time
*
* @author Boris Schauerte
*/
template <typename T>
class MessageQueue
{
public:
  MessageQueue(size_t n)
    : size(0), n(n)
  {
  }
  ~MessageQueue(void)
  {
  }

  /* Get and remove the current message. */
  T GetMessage(void)
  {
    boost::mutex::scoped_lock lock(mutex);
    if (size <= 0)	// IsEmpty() can not be used, due to lock
      throw EmptyMessageQueueException();
    T element = elements.front();
    elements.pop();
    --size;
    if (size == (n - 1)) // was full
      syncNotFull.NotifyAll();
    return element;
  }
  /* Add a new message. */
  void AddMessage(const T msg)
  {
    boost::mutex::scoped_lock lock(mutex);
    if (size >= n)	// IsFull() can not be used, due to lock
      throw FullMessageQueueException();
    elements.push(msg);
    ++size;
    if (size == 1) // was empty
      syncNotEmpty.NotifyAll();
  }
  /* Is the message queue empty? */
  bool IsEmpty(void) const
  {
    boost::mutex::scoped_lock lock(mutex);
    return (size <= 0 ? true : false);
  }
  /* Is the message queue full? */
  bool IsFull(void) const
  {
    boost::mutex::scoped_lock lock(mutex);
    return (size >= n ? true : false);
  }
  /* Get the current size (number of items).*/
  size_t GetSize(void) const
  {
    boost::mutex::scoped_lock lock(mutex);
    return size;
  }
  /* Get the total capacity (max. number of items). */
  size_t GetCapacity(void) const
  {
    return n;
  }

  /** Wait until buffer is not empty. */
  void WaitNotEmpty(void) { syncNotEmpty.Wait(); }
  /** Wait until buffer is not full. */
  void WaitNotFull(void) { syncNotFull.Wait(); }
protected:
  mutable boost::mutex mutex;

  size_t size;  // used instead of elements.size()
  const size_t n;
  
  std::queue<T> elements;

  DataSync syncNotEmpty;
  DataSync syncNotFull;
};
