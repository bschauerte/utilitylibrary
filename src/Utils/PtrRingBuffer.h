/**
 * Copyright 2009 B. Schauerte. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 *    1. Redistributions of source code must retain the above copyright 
 *       notice, this list of conditions and the following disclaimer.
 * 
 *    2. Redistributions in binary form must reproduce the above copyright 
 *       notice, this list of conditions and the following disclaimer in 
 *       the documentation and/or other materials provided with the 
 *       distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY B. SCHAUERTE ''AS IS'' AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL B. SCHAUERTE OR CONTRIBUTORS BE LIABLE 
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *  
 * The views and conclusions contained in the software and documentation
 * are those of the authors and should not be interpreted as representing 
 * official policies, either expressed or implied, of B. Schauerte.
 */

#pragma once

#include "_ul_base_def.h"
#include "Pool.h"

// @todo: check if virtual should be avoided to save performance
//        ... well, I'm using polymorphic code at some places (maybe
//            it is better to replace this later, after all interfaces
//            are stable).

/**
 * @class PtrRingBuffer
 *
 * Special RingBuffer to support pointers.
 * This is a template-class.
 *
 * @author Boris Schauerte
 * @date 2009
 */
template <typename T>
class PtrRingBuffer
{
public:
  PtrRingBuffer(size_t size)
    : n(size), pos(-1), autoDelete(true), count(0)
  {
    pos %= n;
    buffer = new T*[n];
    for (size_t i = 0; i < n; i++)
      buffer[i] = NULL;
  }

  virtual ~PtrRingBuffer(void)
  {
    if (autoDelete)
      for (size_t i = 0; i < n; i++)
        SAFE_DELETE(buffer[i]);
    if (buffer)
    {
      delete [] buffer;
      buffer = NULL;
    }
  }

  /** Get an element
   * @param i i=0 means the most actual (last added) element, i=n-1 is the oldest element
   */
  virtual const T* GetElement(size_t i = 0) const
  {
    i %= n;
    return (buffer[(pos - i) % n]);
  }
  /** Set an element at the specified position.
   * @param i i=0 means the most actual (last added) element, i=n-1 is the oldest element
   */
  virtual T* SetElement(size_t i, T* element)
  {
    if (autoDelete)
      SAFE_DELETE(buffer[(pos - i) % n]);
    T* oldElement = buffer[(pos - i) % n];
    buffer[(pos - i) % n] = element;
    return oldElement;
  }
  /** Add an element to the ring buffer. */
  virtual void AddElement(T* element)
  {
    pos = (pos + 1) % n;
    SetElement(0, element);
    count++;
  }
  /** Sets the actual element to the next element (pos = pos + 1). */
  virtual void Next(void) { pos++; }
  /** Get the current ring buffer position. */
  virtual size_t GetPos(void) const { return pos; }
  /** Get autodelete status. */
  virtual bool GetAutoDelete(void) const { return autoDelete; }
  /** Turn on/off autodelete. */
  virtual void SetAutoDelete(bool value) { autoDelete = value; }
  /** Returns overall number of inserted items. Only AddElement-calls are watched,
   * any abuse of Next/SetElement is ignored and can lead to errors.
   */
  virtual unsigned long int GetCount(void) const { return count; };
  /** Is the ring buffer full/filled? */
  virtual bool isFilled(void) const { return (GetCount() >= n ? true : false); };
protected:
  size_t n;           // size of the ringbuffer
  size_t pos;         // position of the actual element
  T** buffer;         // array holding the elements
  bool autoDelete;    // automatically delete the elements?
  unsigned long int count; // overall number of inserted items

private:
};


/**
 * @class PoolPtrRingBuffer
 *
 * Special implementation of PtrRingBuffer to support pools.
 * Necessary to call pool.Release instead of delete.
 *
 * @author Boris Schauerte
 * @date 2009
 */
/* Note:
 * - the massive use of PtrRingBuffer<T>:: or this-> (BaseClass<Type>::,this->) is necessary,
 *   because the gcc seems to  have problems with template classes and derived classes (Pool
 *   is another example)
 */
template <typename T>
class PoolPtrRingBuffer
  : public PtrRingBuffer<T>
{
public:
  PoolPtrRingBuffer(size_t size, Pool<T>& _pool)
    : PtrRingBuffer<T>(size), pool(_pool)
  {
  }
  virtual ~PoolPtrRingBuffer(void)
  {
  }

  /* Get an element
   * @param i i=0 means the most actual (last added) element, i=n-1 is the oldest element
   */
  virtual const T* GetElement(size_t i = 0) const
  {
    i %= this->n;
    return (this->buffer[(this->pos - i) % this->n]);
  }
  virtual T* SetElement(size_t i, T* element)
  {
    if (this->autoDelete)
      _SAFE_POOL_RELEASE(pool,this->buffer[(this->pos - i) % this->n]);
    T* oldElement = this->buffer[(this->pos - i) % this->n];
    this->buffer[(this->pos - i) % this->n] = element;
    return oldElement;
  }
  virtual void AddElement(T* element)
  {
    this->pos = (this->pos + 1) % this->n;
    SetElement(0, element);
    this->count++;
  }
  /* Sets the actual element to the next element (pos = pos + 1). */
  virtual void Next(void) { this->pos++; }
  virtual size_t GetPos(void) const { return this->pos; }
  /* Get autodelete status. */
  virtual bool GetAutoDelete(void) const { return this->autoDelete; }
  /* Turn on/off autodelete. */
  virtual void SetAutoDelete(bool value) { this->autoDelete = value; }
  /* Returns overall number of inserted items. Only AddElement-calls are watched,
   * any abuse of Next/SetElement is ignored and can lead to errors.
   */
  virtual unsigned long int GetCount(void) const { return this->count; };
  virtual bool isFilled(void) const { return (GetCount() >= this->n ? true : false); };
protected:
  Pool<T>& pool;
private:
};
