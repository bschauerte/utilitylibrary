/**
 * Copyright 2009 B. Schauerte. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 *    1. Redistributions of source code must retain the above copyright 
 *       notice, this list of conditions and the following disclaimer.
 * 
 *    2. Redistributions in binary form must reproduce the above copyright 
 *       notice, this list of conditions and the following disclaimer in 
 *       the documentation and/or other materials provided with the 
 *       distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY B. SCHAUERTE ''AS IS'' AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL B. SCHAUERTE OR CONTRIBUTORS BE LIABLE 
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *  
 * The views and conclusions contained in the software and documentation
 * are those of the authors and should not be interpreted as representing 
 * official policies, either expressed or implied, of B. Schauerte.
 */

#pragma once

/* @class SlidingWindow
 *
 * Implement a sliding window calculation.
 * Input has to be sorted (ascending) array arr of values.
 * Access to the output is iteratively implemented using GetNext().
 * The results are index-pairs minIdx,maxIdx with 'arr[maxIdx]-arr[minIdx] <= interval',
 * these index-pairs represent the index sets {minIdx..maxIdx} of data values.
 *
 * The complete (iteratively calculated) result is a set A of index sets X_i
 *
 *
 * Liefert iterativ
 * Options:
 * - without head: ...
 * - without tail: ...
 * - without intermediate: ...
 * - without head+tail+intermediate:
 *   all sets X_i \in A with \exists X_j \in A : X_i \subset \X_j will be suppressed
 *
 * @author Boris Schauerte
 * @date 2009
 */

// @TODO: fix ugly behavior, if head and tail are "connected"
// e.g. for float data[] = { 0.79, 1.57 };
//          unsigned int size = 2;
//          float interval = 0.8;
//          => only window without head+tail : [0.79, 1.57]
//       => SIMPLE SOLUTION, ALWAYS ACTIVATE HEAD+TAIL IF USING INTERMEDIATE

/* EXAMPLE - BEST WAY TO SEE DIFFERENCES BETWEEN OPTIONS - WITH INTERVAL=4

data[] = { 1, 2, 3, 4, 6, 8, 16, 32, 33, 34, 37, 40 };
size = 12;
interval = 4;

SlidingWindow<1,1,1>
 + 1
 + 1 2
 + 1 2 3
 + 1 2 3 4
 + 2 3 4 6
 + 3 4 6
 + 4 6 8
 + 6 8
 + 8
 + 16
 + 32
 + 32 33
 + 33 34 37
 + 34 37
 + 37 40
 + 40
SlidingWindow<1,1,0>
 + 1
 + 1 2
 + 1 2 3
 + 1 2 3 4
 + 2 3 4 6
 + 4 6 8
 + 16
 + 32 33 34
 + 33 34 37
 + 37 40
 + 40
SlidingWindow<1,0,1>
 + 1
 + 1 2
 + 1 2 3
 + 1 2 3 4
 + 2 3 4 6
 + 3 4 6
 + 4 6 8
 + 6 8
 + 8
 + 16
 + 32
 + 32 33
 + 33 34 37
 + 34 37
 + 37 40
SlidingWindow<1,0,0>
 + 1
 + 1 2
 + 1 2 3
 + 1 2 3 4
 + 2 3 4 6
 + 4 6 8
 + 16
 + 32 33 34
 + 33 34 37
 + 37 40
SlidingWindow<0,1,1>
 + 1 2 3 4
 + 2 3 4 6
 + 3 4 6
 + 4 6 8
 + 6 8
 + 8
 + 16
 + 32
 + 32 33
 + 33 34 37
 + 34 37
 + 37 40
 + 40
SlidingWindow<0,1,0>
 + 1 2 3 4
 + 2 3 4 6
 + 4 6 8
 + 16
 + 32 33 34
 + 33 34 37
 + 37 40
 + 40
SlidingWindow<0,0,1>
 + 1 2 3 4
 + 2 3 4 6
 + 3 4 6
 + 4 6 8
 + 6 8
 + 8
 + 16
 + 32
 + 32 33
 + 33 34 37
 + 34 37
 + 37 40
SlidingWindow<0,0,0>
 + 1 2 3 4
 + 2 3 4 6
 + 4 6 8
 + 16
 + 32 33 34
 + 33 34 37
 + 37 40
*/
template <typename T, bool withHead, bool withTail, bool withIntermediate>
class SlidingWindow
{
public:
  SlidingWindow(const T* _data, unsigned int _size, const T _interval)
    : data(_data), interval(_interval), fromIdx(0), toIdx(0), size(_size)
  {
    if (!withHead)
      while (data[toIdx+1] - data[fromIdx] <= interval && toIdx < size-1)
        ++toIdx;
  }
  ~SlidingWindow(void)
  {
  }

  /** Reset to initial window. */
  inline void Reset(void)
  {
    fromIdx = toIdx = 0;
  }
  /** Is there at least one more window (i.e. were not all windows samples)? */
  inline bool HasNext(void)
  {
    return (fromIdx < size);
  }
  /** Calculate the next sliding window. */
  inline void GetNext(unsigned int& _fromIdx, unsigned int& _toIdx)
  {
    // return current window
    _toIdx = toIdx;
    _fromIdx = fromIdx;

    // slide for next solution/window
    if (fromIdx == 0 && toIdx < size-1) // head
    {
      if (withHead)
      {
        ++toIdx;
        if (data[toIdx] - data[fromIdx] > interval)
        {
          while (data[toIdx] - data[fromIdx] > interval)
            ++fromIdx;
          while (data[toIdx+1] - data[fromIdx] <= interval && toIdx < size-1)
            ++toIdx;
        }
      }
      else
      {
        ++toIdx;
        while (data[toIdx] - data[fromIdx] > interval && fromIdx < size)
          ++fromIdx;
        while (data[toIdx+1] - data[fromIdx] <= interval && toIdx < size-1)
          ++toIdx;
      }
    }
    else if (fromIdx > 0 && toIdx < size-1)
    {
      if (withIntermediate == false)
      {
        ++toIdx;
        while (data[toIdx] - data[fromIdx] > interval && fromIdx < size)
          ++fromIdx;
        while (data[toIdx+1] - data[fromIdx] <= interval && toIdx < size-1)
          toIdx++;
      }
      else
      {
        if (fromIdx < toIdx)
        {
          fromIdx++;
          while (data[toIdx+1] - data[fromIdx-1] < interval && toIdx < size-1) // @todo: <= interval?
            ++toIdx;
        }
        else
        {
          ++toIdx;
          while (data[toIdx] - data[fromIdx] > interval)
            ++fromIdx;
        }
      }
    }
    else if (toIdx >= size-1) // tail
    {
      if (withTail)
        fromIdx++; // right border is at the limit, calculate tail - shift left border
      else
        fromIdx = size; // stop condition
    }
  }
protected:
private:
  unsigned int fromIdx, toIdx;
  unsigned int size;
  const T* data;
  const T interval;
};
