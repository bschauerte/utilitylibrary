/**
 * Copyright 2009 B. Schauerte. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 *    1. Redistributions of source code must retain the above copyright 
 *       notice, this list of conditions and the following disclaimer.
 * 
 *    2. Redistributions in binary form must reproduce the above copyright 
 *       notice, this list of conditions and the following disclaimer in 
 *       the documentation and/or other materials provided with the 
 *       distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY B. SCHAUERTE ''AS IS'' AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL B. SCHAUERTE OR CONTRIBUTORS BE LIABLE 
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *  
 * The views and conclusions contained in the software and documentation
 * are those of the authors and should not be interpreted as representing 
 * official policies, either expressed or implied, of B. Schauerte.
 */

#include "_ul_base_def.h"
#include "Thresholding.h"

#include "_ul_assert.h"

#include "stddef.h"

template <typename T, typename R>
void
Threshold(T* values, const R numValues, const T threshold, const int mode, const T minValue, const T maxValue)
{
  _UL_ASSERT(values != NULL);

  switch (mode)
  {
  default:
  case THRESH_BINARY:
    for (R i = 0; i < numValues; i++)
      values[i] = (values[i] > threshold ? maxValue : minValue);
    break;
  case THRESH_BINARY_INV:
    for (R i = 0; i < numValues; i++)
      values[i] = (values[i] > threshold ? minValue : maxValue);
    break;
  case THRESH_TO_MINVAL:
    for (R i = 0; i < numValues; i++)
      values[i] = (values[i] > threshold ? values[i] : minValue);
    break;
  case THRESH_TO_MINVAL_INV:
    for (R i = 0; i < numValues; i++)
      values[i] = (values[i] > threshold ? minValue : values[i]);
    break;
  case THRESH_BINARY_GEQ:
    for (R i = 0; i < numValues; i++)
      values[i] = (values[i] >= threshold ? maxValue : minValue);
    break;
  case THRESH_BINARY_INV_GEQ:
    for (R i = 0; i < numValues; i++)
      values[i] = (values[i] >= threshold ? minValue : maxValue);
    break;
  case THRESH_TO_MINVAL_GEQ:
    for (R i = 0; i < numValues; i++)
      values[i] = (values[i] >= threshold ? values[i] : minValue);
    break;
  case THRESH_TO_MINVAL_INV_GEQ:
    for (R i = 0; i < numValues; i++)
      values[i] = (values[i] >= threshold ? minValue : values[i]);
    break;
  }
}
template void Threshold(float* values, const int numValues, const float threshold, const int mode, const float minValue, const float maxValue);
template void Threshold(float* values, const size_t numValues, const float threshold, const int mode, const float minValue, const float maxValue);

template <typename T, typename S, typename R>
void
Threshold(const T* values, const R numValues, const T threshold, const int mode, S* out, const S minValue, const S maxValue)
{
  _UL_ASSERT(out != NULL);
  _UL_ASSERT(values != NULL);

  switch (mode)
  {
  default:
  case THRESH_BINARY:
    for (R i = 0; i < numValues; i++)
      out[i] = (values[i] > threshold ? maxValue : minValue);
    break;
  case THRESH_BINARY_INV:
    for (R i = 0; i < numValues; i++)
      out[i] = (values[i] > threshold ? minValue : maxValue);
    break;
  case THRESH_TO_MINVAL:
    for (R i = 0; i < numValues; i++)
      out[i] = (values[i] > threshold ? (S)values[i] : minValue);
    break;
  case THRESH_TO_MINVAL_INV:
    for (R i = 0; i < numValues; i++)
      out[i] = (values[i] > threshold ? minValue : (S)values[i]);
    break;
  case THRESH_BINARY_GEQ:
    for (R i = 0; i < numValues; i++)
      out[i] = (values[i] >= threshold ? maxValue : minValue);
    break;
  case THRESH_BINARY_INV_GEQ:
    for (R i = 0; i < numValues; i++)
      out[i] = (values[i] >= threshold ? minValue : maxValue);
    break;
  case THRESH_TO_MINVAL_GEQ:
    for (R i = 0; i < numValues; i++)
      out[i] = (values[i] >= threshold ? (S)values[i] : minValue);
    break;
  case THRESH_TO_MINVAL_INV_GEQ:
    for (R i = 0; i < numValues; i++)
      out[i] = (values[i] >= threshold ? minValue : (S)values[i]);
    break;
  }
}
template void Threshold(const float* values, const int numValues, const float threshold, const int mode, float* out, const float minValue, const float maxValue);
template void Threshold(const float* values, const size_t numValues, const float threshold, const int mode, float* out, const float minValue, const float maxValue);


template <typename T, typename S, typename R>
void
ThresholdBinarize(const T* values, const R numValues, const T threshold, const int mode, S* out, const S minValue, const S maxValue)
{
  _UL_ASSERT(out != NULL);
  _UL_ASSERT(values != NULL);

  switch (mode)
  {
  default:
    _UL_ASSERT(false);
  case THRESH_BINARY:
    for (R i = 0; i < numValues; i++)
      out[i] = (values[i] > threshold ? maxValue : minValue);
    break;
  case THRESH_BINARY_INV:
    for (R i = 0; i < numValues; i++)
      out[i] = (values[i] > threshold ? minValue : maxValue);
    break;
  case THRESH_BINARY_GEQ:
    for (R i = 0; i < numValues; i++)
      out[i] = (values[i] >= threshold ? maxValue : minValue);
    break;
  case THRESH_BINARY_INV_GEQ:
    for (R i = 0; i < numValues; i++)
      out[i] = (values[i] >= threshold ? minValue : maxValue);
    break;
  }
}
template void ThresholdBinarize(const float* values, const int numValues, const float threshold, const int mode, unsigned char* out, const unsigned char minValue, const unsigned char maxValue);
template void ThresholdBinarize(const float* values, const size_t numValues, const float threshold, const int mode, unsigned char* out, const unsigned char minValue, const unsigned char maxValue);
