/**
 * Copyright 2009 B. Schauerte. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 *    1. Redistributions of source code must retain the above copyright 
 *       notice, this list of conditions and the following disclaimer.
 * 
 *    2. Redistributions in binary form must reproduce the above copyright 
 *       notice, this list of conditions and the following disclaimer in 
 *       the documentation and/or other materials provided with the 
 *       distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY B. SCHAUERTE ''AS IS'' AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL B. SCHAUERTE OR CONTRIBUTORS BE LIABLE 
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *  
 * The views and conclusions contained in the software and documentation
 * are those of the authors and should not be interpreted as representing 
 * official policies, either expressed or implied, of B. Schauerte.
 */

#pragma once

/** @file Thresholding.h
 *
 * Thresholding of arrays with support for common thresholding modes.
 *
 * @author Boris Schauerte
 * @date 2009
 */

/** Supported thresholding modes. */
enum eThresholdingModes
{
  THRESH_BINARY = 0,
  THRESH_BINARY_INV,
  THRESH_TO_MINVAL,
  THRESH_TO_MINVAL_INV,
  THRESH_BINARY_GEQ,
  THRESH_BINARY_INV_GEQ,
  THRESH_TO_MINVAL_GEQ,
  THRESH_TO_MINVAL_INV_GEQ,
  THRESH_NUM_MODES
};

/** Apply threshold. Supported thresholding modes are:
     THRESH_BINARY           - val =(val >  Thresh ? maxValue : minValue)
     THRESH_BINARY_INV       - val =(val >  Thresh ? minValue : maxValue)
     THRESH_TOZERO           - val =(val >  Thresh ? val : minValue)
     THRESH_TOZERO_INV       - val =(val >  Thresh ? minValue : val)
     THRESH_BINARY_GEQ       - val =(val >= Thresh ? maxValue : minValue)
     THRESH_BINARY_INV_GEQ   - val =(val >= Thresh ? minValue : maxValue)
     THRESH_TOZERO_GEQ       - val =(val >= Thresh ? val : minValue)
     THRESH_TOZERO_INV_GEQ   - val =(val >= Thresh ? minValue : val)
*/
template <typename T, typename R>
void Threshold(T* values, const R numValues, const T threshold, const int mode, const T minValue = 0, const T maxValue = 1);
/** Apply threshold. Supported thresholding modes are:
     THRESH_BINARY           - val =(val >  Thresh ? maxValue : minValue)
     THRESH_BINARY_INV       - val =(val >  Thresh ? minValue : maxValue)
     THRESH_TOZERO           - val =(val >  Thresh ? val : minValue)
     THRESH_TOZERO_INV       - val =(val >  Thresh ? minValue : val)
     THRESH_BINARY_GEQ       - val =(val >= Thresh ? maxValue : minValue)
     THRESH_BINARY_INV_GEQ   - val =(val >= Thresh ? minValue : maxValue)
     THRESH_TOZERO_GEQ       - val =(val >= Thresh ? val : minValue)
     THRESH_TOZERO_INV_GEQ   - val =(val >= Thresh ? minValue : val)
*/
template <typename T, typename S, typename R>
void Threshold(const T* values, const R numValues, const T threshold, const int mode, S* out, const S minValue = 0, const S maxValue = 1);

/** ThresholdBinarize input through thresholding. Supported thresholding modes are:
     THRESH_BINARY           - val =(val >  Thresh ? maxValue : minValue)
     THRESH_BINARY_INV       - val =(val >  Thresh ? minValue : maxValue)
     THRESH_BINARY_GEQ       - val =(val >= Thresh ? maxValue : minValue)
     THRESH_BINARY_INV_GEQ   - val =(val >= Thresh ? minValue : maxValue)
   Just minValue/maxValue as output (binarization).
*/
template <typename T, typename S, typename R>
void ThresholdBinarize(const T* values, const R numValues, const T threshold, const int mode, S* out, const S minValue = 0, const S maxValue = 1);
