/**
 * Copyright 2009 B. Schauerte. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 *    1. Redistributions of source code must retain the above copyright 
 *       notice, this list of conditions and the following disclaimer.
 * 
 *    2. Redistributions in binary form must reproduce the above copyright 
 *       notice, this list of conditions and the following disclaimer in 
 *       the documentation and/or other materials provided with the 
 *       distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY B. SCHAUERTE ''AS IS'' AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL B. SCHAUERTE OR CONTRIBUTORS BE LIABLE 
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *  
 * The views and conclusions contained in the software and documentation
 * are those of the authors and should not be interpreted as representing 
 * official policies, either expressed or implied, of B. Schauerte.
 */

#include "_ul_base_def.h"
#include "StringUtils.h"

#include "boost/tokenizer.hpp" // for Tokenize

#include <iostream>

void
ParseDynamicParams(const std::string& params, ParamMap_t& result, const char* sepItem, const char sepNVP)
{
  std::vector<std::string> tokens = Tokenize(params, sepItem);
  for (std::vector<std::string>::iterator it = tokens.begin(); it != tokens.end(); it++)
  {
    std::string name, value;
    std::string word = (*it);
    size_t pos = word.find_first_of(sepNVP);
    if (pos != std::string::npos)
    {
      name = word.substr(0,pos);
      value = word.substr(pos+1);
      std::string lname(name);
      boost::algorithm::to_lower(lname);
      result[lname] = value;
    }
    else
    {
      // error
      std::cerr << "ERROR: ParseDynamicParams, parsing error (" << params << ")" << std::endl;
      std::cerr << "       valid format: name1[=]value1[,]name2[=]value2[,]...[,]nameN[=]valueN" << std::endl;
      std::cerr << "       with [=] as '" << sepNVP << "' and [,] as '" << sepItem << "'" << std::endl;
      exit(0);
    }
  }
}

std::vector<std::string> 
Tokenize(const std::string& s, const char* char_separator)
{
  typedef boost::tokenizer<boost::char_separator<char> > Tok; // Tokenizer for string parsing
  
  boost::char_separator<char> sep(char_separator);
  Tok tok(s, sep);
  Tok::iterator tok_iter = tok.begin();
  return std::vector<std::string>(tok.begin(), tok.end());
}

bool
IsNumber(const std::string& s)
{
  for (std::string::size_type i = 0; i < s.size(); i++)
    if (!std::isdigit(s[i]))
      return false;
  return true;
}
