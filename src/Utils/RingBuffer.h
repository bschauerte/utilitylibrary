/**
 * Copyright 2009 B. Schauerte. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 *    1. Redistributions of source code must retain the above copyright 
 *       notice, this list of conditions and the following disclaimer.
 * 
 *    2. Redistributions in binary form must reproduce the above copyright 
 *       notice, this list of conditions and the following disclaimer in 
 *       the documentation and/or other materials provided with the 
 *       distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY B. SCHAUERTE ''AS IS'' AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL B. SCHAUERTE OR CONTRIBUTORS BE LIABLE 
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *  
 * The views and conclusions contained in the software and documentation
 * are those of the authors and should not be interpreted as representing 
 * official policies, either expressed or implied, of B. Schauerte.
 */

#pragma once

#include "_ul_base_def.h"

#include <stddef.h>
#include <algorithm>

/**
 * @class RingBuffer
 *
 * A template ring buffer class (see http://en.wikipedia.org/wiki/Circular_buffer).
 *
 * @author Boris Schauerte
 * @date 2009
 */
template <typename T>
class RingBuffer
{
public:
	RingBuffer(size_t _size)
	  : n(_size), pos(0), count(0)
	{
	  buffer = new T[_size];
	}
	~RingBuffer(void) { if (buffer != NULL) delete [] buffer; }

	/** Add x to the ring buffer. */
	inline void AddElement(T x)
	{
    buffer[pos] = x;
    pos = (pos + 1) % n;
    ++count;
	}
	/** Add x to the ring buffer. */
	inline void Add(T x) { AddElement(x); }
	/** Get the current (i=0) element from the buffer. Or get any element of the buffer (i!=0). */
	inline T GetElement(size_t i = 0) const
	{
	  i %= n;
	  return (buffer[(pos - i) % n]);
	}
	/** Get the current (i=0) element from the buffer. Or get any element of the buffer (i!=0). */
	inline T Get(size_t i = 0) const { return GetElement(i); }

	// Calculate element statistics
	/** Calculate the average of current elements */
	inline T Average(void) const
	{
	  size_t numElements = (std::min)(count, n); // (std::min)(...) used to avoid problems with windows.h macro definitions
	  T tmp = Sum();
	  tmp /= numElements;
	  return tmp;
	}
	/** Calculate the sum of current elements */
	inline T Sum(void) const
	{
	  size_t numElements = (std::min)(count, n); // (std::min)(...) used to avoid problems with windows.h macro definitions
	  T tmp = (T)0;
	  for (size_t i = 0; i < numElements; i++)
	    tmp += buffer[i];
	  return tmp;
	}

	/** Get overall number of inserted items */
  inline size_t GetCount(void) const { return count; };
  /** Is the buffer "filled" with elements (i.e. at least n elements were added)? */
  inline bool IsFilled(void) const { return (count >= n ? true : false); };
  /** Get size of the ring buffer. */
  inline size_t Size(void) const { return n; }
protected:
private:
  T* buffer;
  size_t count;            // overall number of inserted items
  size_t n;                // size of the ringbuffer
  size_t pos;              // position of the actual element
};
