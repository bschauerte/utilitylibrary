/**
 * Copyright 2009 B. Schauerte. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 *    1. Redistributions of source code must retain the above copyright 
 *       notice, this list of conditions and the following disclaimer.
 * 
 *    2. Redistributions in binary form must reproduce the above copyright 
 *       notice, this list of conditions and the following disclaimer in 
 *       the documentation and/or other materials provided with the 
 *       distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY B. SCHAUERTE ''AS IS'' AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL B. SCHAUERTE OR CONTRIBUTORS BE LIABLE 
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *  
 * The views and conclusions contained in the software and documentation
 * are those of the authors and should not be interpreted as representing 
 * official policies, either expressed or implied, of B. Schauerte.
 */

/** @file CommonMacros.h
 *
 * Collection of useful macros.
 *
 * @author B. Schauerte
 * @date 2009
 */

#pragma once

/** NULL=(0)
 *
 * Use NULL for pointers and 0 for numbers. This makes the code more readable/understandable,
 * because pointers and numbers are easier to distinguish.
 */
#ifndef NULL
#define NULL (0)
#endif

/** Safe delete */
#ifndef SAFE_DELETE
#define SAFE_DELETE(ptr) { if (ptr != NULL) { delete ptr; ptr = NULL; } }
#endif
/** Safe delete array */
#ifndef SAFE_DELETE_ARRAY
#define SAFE_DELETE_ARRAY(arr_ptr) { if (arr_ptr != 0) delete [] arr_ptr; arr_ptr = NULL; }
#endif
/** Suppress "unused variable warning" */
#ifndef UNUSED_VARIABLE
#define UNUSED_VARIABLE(x) ((void)(x))
#endif

/** Macros for abstract classes and interfaces */
#if defined _WIN32 || defined WIN32
#define INTERFACE struct __declspec(novtable)
#else
#define INTERFACE struct
#endif
#if defined _WIN32 || defined WIN32
#define ABSTRACT class __declspec(novtable)
#else
#define ABSTRACT class
#endif
