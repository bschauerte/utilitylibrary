/**
 * Copyright 2009 B. Schauerte. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 *    1. Redistributions of source code must retain the above copyright 
 *       notice, this list of conditions and the following disclaimer.
 * 
 *    2. Redistributions in binary form must reproduce the above copyright 
 *       notice, this list of conditions and the following disclaimer in 
 *       the documentation and/or other materials provided with the 
 *       distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY B. SCHAUERTE ''AS IS'' AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL B. SCHAUERTE OR CONTRIBUTORS BE LIABLE 
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *  
 * The views and conclusions contained in the software and documentation
 * are those of the authors and should not be interpreted as representing 
 * official policies, either expressed or implied, of B. Schauerte.
 */

#pragma once

#include "_ul_base_def.h"

#include <string>
#include <sstream>

#define _ENABLE_TO_STRING_CSTDINT_EXTENSIONS
#ifdef _ENABLE_TO_STRING_CSTDINT_EXTENSIONS
#include "boost/cstdint.hpp"
#endif

/** @file ToString.h
 *
 * Convert numbers to string.
 * Implemented using C++ methods (no use of atoi, etc.).
 *
 * Explicit types, no use of template.
 *
 * @author Boris Schauerte
 * @date 2009
 */

/** Convert float to string. */
std::string ToString(float f);
/** Convert double to string. */
std::string ToString(double d);
/** Convert int to string. */
std::string ToString(int i);
/** Convert unsigned int to string. */
std::string ToString(unsigned int i);
/** Convert short to string. */
std::string ToString(short i);
/** Convert unsigned short to string. */
std::string ToString(unsigned short i);
/** Convert string to string. */
std::string ToString(const std::string& s); // Useful in some situations (e.g. to keep macros independent from type - no differentiation needed between string and numbers)

std::string ToString(const time_t& t);

#ifdef _ENABLE_TO_STRING_CSTDINT_EXTENSIONS
/** Convert unsigned boost::uint64_t to string. */
std::string ToString(boost::uint64_t i);
/** Convert unsigned boost::int64_t to string. */
std::string ToString(boost::int64_t i);
#endif

template <typename T>
inline std::string
_ToString(const T& t)
{
  std::ostringstream s;
  s << t;
  return s.str();
}
