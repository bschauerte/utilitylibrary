/**
 * Copyright 2009 B. Schauerte. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 *    1. Redistributions of source code must retain the above copyright 
 *       notice, this list of conditions and the following disclaimer.
 * 
 *    2. Redistributions in binary form must reproduce the above copyright 
 *       notice, this list of conditions and the following disclaimer in 
 *       the documentation and/or other materials provided with the 
 *       distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY B. SCHAUERTE ''AS IS'' AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL B. SCHAUERTE OR CONTRIBUTORS BE LIABLE 
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *  
 * The views and conclusions contained in the software and documentation
 * are those of the authors and should not be interpreted as representing 
 * official policies, either expressed or implied, of B. Schauerte.
 */

/** @file fops.h
 *
 * Simple definition of the often wished and missed
 * file handling functionality for C/C++.
 *
 * Includes
 * - fexists(...)
 * - mkdir(...)
 *
 * Platform independent, of course ;)
 *
 * @author Boris Schauerte
 * @date 2009
 */
#pragma once

#ifdef _FEXISTS_USE_BOOST_FILESYSTEM
#include "boost/filesystem/operations.hpp" // includes boost/filesystem/path.hpp
#include "boost/filesystem/fstream.hpp"    // ditto

inline bool
fexists_boost_fsys(const char *filename)
{
  return boost::filesystem::exists(filename);
}
inline bool
fexists_boost_fsys(const std::string& filename)
{
  return boost::filesystem::exists(filename);
}
#endif

#include <fstream>

inline bool
fexists_ifstream(const char *filename)
{
  std::ifstream ifile(filename);
  return ifile;
}
inline bool
fexists_ifstream(const std::string& filename)
{
  std::ifstream ifile(filename.c_str());
  return ifile;
}

#ifndef fexists // already defined?
#ifdef _FEXISTS_USE_BOOST_FILESYSTEM
#define fexists(__filename) boost::filesystem::exists(__filename) //fexists_boost_fsys(x)
#else
#define fexists(__filename) fexists_ifstream(__filename)
#endif
#endif

#if defined( _WIN32 ) || defined( WIN32 )
#include <direct.h>
inline bool
fmkdir_direct(const std::string& path)
{
  return fmkdir_direct(path.c_str());
}
inline bool
fmkdir_direct(const char* path)
{
  return (mkdir(path) != -1);
}
#else
#include <sys/types.h>
#include <sys/stat.h>
inline bool
fmkdir_direct(const char* path)
{
  return (mkdir(path, S_IRWXU | S_IRWXG | S_IRWXO) != -1);
}
inline bool
fmkdir_direct(const std::string& path)
{
  return fmkdir_direct(path.c_str());
}
#endif

#ifndef fmkdir
#ifdef _FEXISTS_USE_BOOST_FILESYSTEM
#define fmkdir(__path) boost::filesystem::create_directory(__path)
#else
#define fmkdir(__path) fmkdir_direct(__path)
#endif
#endif
