/**
 * Copyright 2009 B. Schauerte. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 *    1. Redistributions of source code must retain the above copyright 
 *       notice, this list of conditions and the following disclaimer.
 * 
 *    2. Redistributions in binary form must reproduce the above copyright 
 *       notice, this list of conditions and the following disclaimer in 
 *       the documentation and/or other materials provided with the 
 *       distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY B. SCHAUERTE ''AS IS'' AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL B. SCHAUERTE OR CONTRIBUTORS BE LIABLE 
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *  
 * The views and conclusions contained in the software and documentation
 * are those of the authors and should not be interpreted as representing 
 * official policies, either expressed or implied, of B. Schauerte.
 */

#pragma once

#include <vector>
#include <map>
#include <string>
#include <sstream>
#include <algorithm>

#include "boost/algorithm/string.hpp"

typedef std::map<std::string,std::string> ParamMap_t;
typedef std::map<std::string,std::string>::iterator ParamMap_it_t;

#define _READ_PARAM_MAP(name,map) {\
  std::string lname( #name );\
  boost::algorithm::to_lower(lname);\
  ParamMap_it_t __it = map.find( lname );\
  if (__it != map.end())\
{\
  std::string value = __it->second;\
  std::stringstream ss(value);\
  ss >> name;\
}\
}

#define _READ_PARAM_MAP_NAMED(varname,name,map) {\
  std::string lname( name );\
  boost::algorithm::to_lower(lname);\
  ParamMap_it_t __it = map.find( lname );\
  if (__it != map.end())\
{\
  std::string value = __it->second;\
  std::stringstream ss(value);\
  ss >> varname;\
}\
}

/** @file StringUtils.h
 *
 * (Widely used) Utilities for string processing
 * - parse name=value pairs
 * - tokenize a string (split into separated parts)
 * - check whether a string is a number
 *
 * @author Boris Schauerte
 * @date 2009
 */

/** Parse a given string into name=value pairs (useful for dynamic parameters). */
void ParseDynamicParams(const std::string& params, ParamMap_t& result, const char* sepItem = " ", const char sepNVP = '=');

/** Tokenize the string into a vector. */
std::vector<std::string> Tokenize(const std::string& s, const char* char_separator);
/** Represents string s a number? (you have to trim first! it does not ignore whitespaces, etc.). */
bool IsNumber(const std::string& s);
