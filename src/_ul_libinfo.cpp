/**
 * Copyright 2009 B. Schauerte. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 *    1. Redistributions of source code must retain the above copyright 
 *       notice, this list of conditions and the following disclaimer.
 * 
 *    2. Redistributions in binary form must reproduce the above copyright 
 *       notice, this list of conditions and the following disclaimer in 
 *       the documentation and/or other materials provided with the 
 *       distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY B. SCHAUERTE ''AS IS'' AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL B. SCHAUERTE OR CONTRIBUTORS BE LIABLE 
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *  
 * The views and conclusions contained in the software and documentation
 * are those of the authors and should not be interpreted as representing 
 * official policies, either expressed or implied, of B. Schauerte.
 */

#include "_ul_base_def.h"
#include "_ul_libinfo.h"

namespace ul
{
  const char*
  _LIB_BUILD_OPTIONS(void)
  {
    return "USE_BOOST_THREAD_SLEEP="
#ifdef USE_BOOST_THREAD_SLEEP
      "true"
#else
      "false"
#endif
      ",USE_LOG_MESSAGE="
#ifdef USE_LOG_MESSAGE
      "true"
#else
      "false"
#endif
      ",BOOST_DISABLE_ASSERTS="
#ifdef BOOST_DISABLE_ASSERTS
      "true"
#else
      "false"
#endif
    ",DISABLE_UL_ASSERTS="
#ifdef DISABLE_UL_ASSERTS
    "true"
#else
    "false"
#endif
      ;
  }

  const char*
  _LIB_BUILD_VERSION(void)
  {
    return "dev-1.0";
  }

  const char*
  _LIB_BUILD_NAME(void)
  {
    return "UtilityLibrary";
  }

  const char*
  _LIB_BUILD_DATE(void)
  {
    return __DATE__;
  }

  const char*
  _LIB_BUILD_TIME(void)
  {
    return __TIME__;
  }
}
