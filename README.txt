This is a C/C++ library containing various utility/tool functions and classes
that I implemented for my Diplom thesis. This code is part of the framework
that has been described in

[1] B. Schauerte, T. Plötz, G. A. Fink, "A Multi-modal Attention System for
    Smart Environments". In Proc. 7th International Conference on Computer
    Vision Systems (ICVS), Lecture Notes in Computer Science, LNCS 5815, pp. 
    73-83, Springer, Liege, Belgium, October 13-15, 2009.

[2] B. Schauerte, J. Richarz, T. Plötz, C. Thurau, G. A. Fink, "Multi-Modal 
    and Multi-Camera Attention in Smart Environments". In Proc. 11th 
    International Conference on Multimodal Interfaces (ICMI)1, pp. 261-268, 
    ACM, Cambridge, MA, USA, November 2-4, 2009.

If you use and like any of the code, please acknowledge my work by citing the
papers above and/or keeping/distributing the copyright statements in/with your
software.

Kind regards
Boris Schauerte
http://cvhci.anthropomatik.kit.edu/~bschauer/
